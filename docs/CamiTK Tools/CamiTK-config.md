camitk-config is a command line application that helps you discover what
is available on your installed version of CamiTK. it gives your
information about the installation paths, the extension repositories,
how many extensions of which types are installed etc…

For example in CamiTK 4.2, you have the following possible options:
```bash
:$ camitk-config

Usage: camitk-config [options]
Build using CamiTK 4.2.dev.develop.215f2654

Options:
--help|-h                         Print usage information and exit.
--version|-v                      Print CamiTK version message
--config|-c                       Print all information for a complete CamiTK
                                  diagnosis and exit
--print-paths|-p                  Print CamiTK paths on the standard output and
                                  exit
--camitk-dir|-d|--camitkDir       Print CAMITK_DIR (the installation directory)
                                  and exit
--short-version|-s|--shortVersion Print CamiTK short version string
--complete-version|-vv            Print CamiTK complete version number
                                  (including patch number)
--time-stamp|-t                   Generate a time stamp in format
                                  YYYY-MM-DDTHH:mm:ss from current system date
                                  and time
--bug-report-info|-b              Generate a report bug template with the CamiTK
                                  diagnosis in it

```

This is an exemple of `camitk-config --config` output:
```bash
- CamiTK version........................... CamiTK 4.2.dev.develop.215f2654
- CamiTK Short Version..................... camitk-4.2
- CamiTK SO NAME........................... 4
- Operating System......................... LINUX
- Build type............................... RELEASE
- QT Version............................... 5.7.1
- VTK Version.............................. 6.3.0
- Global Installation Directory [G]........ [locally installed version: using user config directory as repository]
- Local Installation Directory [L]......... ~/.config/CamiTK
- Current Working Directory [W]............ ~/tmp_migration_wiki/migration
- Test Data Directory...................... ~/.config/CamiTK/share/camitk-4.2/testdata
- Component Extension Directories.......... ~/.config/CamiTK/lib/camitk-4.2/components
- Action Extension Directories............. ~/.config/CamiTK/lib/camitk-4.2/actions
- Number of Component Extensions........... 14 (locations: 0 global, 14 local, 0 in working directory, 0 manually installed by user)
- Number of File Extensions Supported...... 37
- Number of Action Extensions.............. 27 (locations: 0 global, 27 local, 0 in working directory, 0 manually installed by user)
- Number of Actions........................ 105
- Registered components:
  - [L] Alias Wavefront OBJ Component...... obj
  - [L] ItkImages Component................ hdr, spr, gipl, pic, lsm, nrrd, hdr.gz, nii, nii.gz, img, img.gz
  - [L] MML Component...................... mml, scn
  - [L] Msh Component...................... msh
  - [L] Off Component...................... off
  - [L] PML Component...................... pml
  - [L] Pick Me Component.................. pickme
  - [L] STL Component...................... stl, STL
  - [L] Testing Abort Component............ abort
  - [L] Testing Mixed Component............ mixed
  - [L] VRML 2 Component................... vrml, wrl
  - [L] VTK Component...................... vtk
  - [L] vtkImages Component................ jpg, png, tiff, tif, bmp, pbm, pgm, ppm, mhd, mha, raw
  - [L] DICOM.............................. directory
- Registered actions:
  - [L] Application Level Actions.......... 21 actions
  - [L] AverageVoxelValuesExtension........ 1 actions
  - [L] Basic Mesh Extension............... 9 actions
  - [L] Basic Picking Tutorial............. 1 actions
  - [L] Basic Topology..................... 2 actions
  - [L] BoxVOIExtension.................... 1 actions
  - [L] Frame Edition Extension............ 1 actions
  - [L] ITK Filters........................ 14 actions
  - [L] ITK Segmentation................... 3 actions
  - [L] Image LUT.......................... 1 actions
  - [L] ImageAcquisitionActionExtension.... 7 actions
  - [L] MML................................ 2 actions
  - [L] Mesh Point Data.................... 1 actions
  - [L] Mesh Processing.................... 17 actions
  - [L] Mesh Selection..................... 1 actions
  - [L] MultiPickingExtension.............. 1 actions
  - [L] PMLExploreExtension................ 2 actions
  - [L] Pixel Color Changer................ 1 actions
  - [L] PropertiesExtension................ 3 actions
  - [L] Reconstruction..................... 1 actions
  - [L] Reorient Image Extension........... 1 actions
  - [L] ResampleExtension.................. 1 actions
  - [L] Shaker Extension................... 3 actions
  - [L] ShowIn3DExtension.................. 5 actions
  - [L] Sleeping While Working............. 2 actions
  - [L] VolumeRenderingExtension........... 1 actions
  - [L] Vtk Widget Tutorial................ 2 actions

```
