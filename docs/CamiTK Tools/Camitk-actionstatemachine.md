This application takes an action pipeline (*SCXML*) file and plays it
back. It is a wizard-type application that guides the user through
predefined tasks (always the same, always in the same order) with
predefined values (that might be changed interactively by the user).

![CamiTK-Imp](./assets/Camitk-actionstatemachine.png)

If you want to use CamiTK Action State Machine for your project, check
the documentation below.

!!! note
    a small documentation written by Polytech Health Information Technology students

# Action State Machine’s guide

If you want to use an automatic execution protocol for your actions like
the Action State Machine (ASM), this guide can help you.

## Create an ASM model

If you want to generate a model for your ASM, it is possible to record
your actions executed in CamiTK into a .xml file. To do so, you have to
execute your actions in CamiTK, and once you have finished, click on
File → Save history.

![CamiTK-Imp](./assets/Save-history-asm.png)

A .xml file is generated, save it. Your ASM can be saved for example in
an ASM subdirectory in your CEP source tree project (ex : Exploranat).

![CamiTK-Imp](./assets/Save-asm-left.png)
![CamiTK-Imp](./assets/Save-asm-right.png)

Now, you can open this file and use it like a model for your ASM.
However, this file is only a model, using it without any modification is
not possible: it probably won’t work.

## Start an ASM

To start your ASM just
type:

```bash
camitk-actionstatemachine -f /path/to/Exploranat/ASM/protocole.xml -o /tmp
```

Your ASM starts automatically:

![CamiTK-Imp](./assets/Asm-example.png)

#### Note

If you have developed your own extensions, either installed them in the
proper extension repository or run the action state machine application
from your build directory:

``` bash
cd /path/to/myCEP-build-dir
camitk-actionstatemachine -f /path/to/Exploranat/ASM/protocole.xml -o /tmp
```

## Example of camitk action state machine .xml file
![CamiTK-Imp](./assets/Asm-start.png)
![CamiTK-Imp](./assets/Asm-middle.png)
![CamiTK-Imp](./assets/Asm-end.png)
