![CamiTK-Imp](./assets/Camitk-imp.png)

camitk-imp is the flagship application of CamiTK. It can be considered
as the CamiTK workbench. It provides an easy and interactive access to
all data and parameters, all the installed extensions. The targeted
users are R\&D developers who wants to test their work, improve their
algorithm, build a complete application, etc… You can have a better
understanding of what camitk-imp can do for you by watching the
[demonstration videos](http://camitk.imag.fr/userTeaser.php).

[Hands on](http://camitk.free.fr//tutorials/camitk/user-01/index.html)

[Medical imaging and biomechanical modeling using camitk-imp](http://misr.free.fr)
