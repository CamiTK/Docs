The Wizard is a tool that helps to initiate for the creation and design of a CamiTK Extension Project.
The user provides information to the wizard, which automatically generates the code of the extension structure and skeleton source code of the required C++ classes. 

A CamiTK Extension Project (CEP) can contain one or several types of extensions.

The wizard is a one shot, once it has created the initial CEP, it is up to you to modify it, add or remove extension manually.

At the moment, the wizard allows you to initialize CEP that contains:

* New Component(s) for handling new type of data for instance (the wizard generates a Component extension class and the associated top-level Component class)
* New Action(s) for doing some customized operations on the components (the wizard generates an Action extension class and one or more class extending the generic Action class)
* New Viewer(s) for the visualization of data (the viewer generates a Viewer extension class and one or more named viewers)

The wizard guides you during the creation steps and creates an XML description of your CEP. 
Once finished it calls `camitk-cepgenerator` which generates the CEP skeleton, including `CMakeLists.txt` (the files needed by the CMake build system).

You end up with a first initial version of a CEP tree and source code, ready to compile!
After that, you have to run CMake in order to configure and generate the files required to build your CEP.
Note that the initial skeleton generated code is valid and compiles as it is.
At runtime, camitk applications will be able to load your extension and use it immediately.

And now let the coding fun start...

