!!! warning
    This is a work in progress. |

For some CamiTK actions, displaying new elements in the viewers might be
needed.

## How to create a new component and add it to a viewer

To complete with a nice solution.

## How to display the output of a vtk filter

This method avoid the creation of a new Component : the output of a vtk
filter can be displayed by only changing the data connection of the
initial Component.

Example with a clip filter :

```c++
MeshComponent *your_mesh; // the initial mesh
vtkSmartPointer < vtkClipDataSet > filter = vtkSmartPointer < vtkClipDataSet >::New();
filter->SetInputConnection(mesh->getDataPort());
filter->SetClipFunction(your_plane);
filter->Update();
mesh->setDataConnection(filter->GetOutputPort());
```

The initial mesh is unchanged, there is no need saving it before apply
the filter.
