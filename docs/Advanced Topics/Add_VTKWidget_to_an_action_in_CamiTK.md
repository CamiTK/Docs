<!--
    !!! warning
    This is a work in progress. http://camitk.imag.fr/
-->

VTK widgets are useful for interactive actions. They can be easily added in CamiTK actions.
This page gives you a step by step to add an interactive VTK widget in the default 3D viewer in an action.

## Creating a class for your widget

The first step is the creation of a class for the VTK widget. This class
must inherit from the vtkCommand class.

Example with a vtkImplicitPlaneWidget :

```c++
using namespace camitk;
class PlaneWidget : public vtkCommand {
    public:
        PlaneWidget();
        virtual ~PlaneWidget();
        vtkSmartPointer<vtkImplicitPlaneWidget> planeWidget; // Your VTK Widget
        virtual void Execute(vtkObject *caller, unsigned long, void *); // Inherited from vtkCommand
};
```

## Adding your widget to your CamiTK action

Once your VTK widget is created, you need to add it in your header file :

```c++
class YourAction : public camitk::Action {
    public:
        YourAction(ActionExtension *); // The constructor.
        virtual QWidget *getWidget(); // Method called when the action when the action is triggered (i.e. started).
    public slots:
        virtual ApplyStatus apply(); // Method called when the action is applied.
    private:
        PlaneWidget *widget; // Your widget
};
```

Then, you must implement the getWidget() method :

```c++
QWidget *YourAction::getWidget() {
    // Creating the clipping plane widget (the first time getWidget() is called)
    if(!widget->planeWidget) {
        vtkRenderWindowInteractor *iren = InteractiveViewer::get3DViewer()->getRendererWidget()->GetRenderWindow()->GetInteractor();
        widget->planeWidget = vtkSmartPointer < vtkImplicitPlaneWidget >::New();
        widget->planeWidget->SetInteractor(iren);

        // Set the properties of the widget
        ...

        widget->planeWidget->AddObserver(vtkCommand::EndInteractionEvent,this->widget);
    }
    return Action::getWidget();
}
```
