![External Libraries](./assets/Extlib.png)

As you know CamiTK depends on
some external C++ libraries, such as Qt and VTK. Some other external
libraries may be required for special CEP, such as ITK for the CEP
IMAGING. This page presents the CamiTK external libraries policy. It
deals especially on how we decide to increase an external library
version on which CamiTK depends.

# No quick change

First of all, we don’t change our external libraries versions on which
CamiTK depend, each time a new version is out. This is also available
for any compiler version (especially regarding Microsoft Visual Studio
compiler). This process requires an analysis of a specific need. This is
to say, the whole CamiTK has to approved this need, to decide to
increase the version of the library on which CamiTK depends, that
provides this functionality. Note, that the version number is increased
to the lowest stable version of the lib that features the new desired
functionality.

## Windows criteria

Specific criteria are taken into account for Windows platforms:

  - Select a certain number of supported compiler (not all versions of
    Visual Studio for instance), proportional to the CamiTK team size.
  - Take the automatic installer process (32bits & 64bits) into account,
    when deciding which library to upgrade.

## Linux criteria

  - Follow the Debian packages versions supported: CamiTK should be able
    to run on a Debian stable version, or in a period of transition, the
    Debian testing version.
  - This does not forbid to support higher libraries version than Debian
    stable.

# When do these changes are decided?

Upgrading libraries versions changes are made 1 month before a new
release.
