## Introduction

A dynamic library (.dll, .so, .dylib, named *DLL* in the following
explanation) contains C++ compiled functions which might be called by
another program using this DLL at runtime. When programming, as a
developer you may want to reuse some classes or functions already
defined and compiled in a dynamic library, or, on the other hand, you
would like to make some classes / functions available within a dynamic
library (for other developments).

Regarding C++ programming with dynamic libraries, DLL import / export
refers to this following specification :

-   **import** : Using classes / methods defined in an already compiled
    dynamic library
-   **export** : Defining classes / methods to be callable in a compiled
    DLL

Although this mechanism only concerns Windows compilation using MSVC, as
CamiTK uses the same code for all the plateforms, you will need to
follow the instructions given here in order to provide a multi-plateform
code.

## How to export in a DLL your classes / methods in C++ ?

To make available your class or function available in a DLL, you need to
use the following macro as a prefix of your class / function name :

```c++
__declspec(dllexport)
```

This will tell the compiler that this class / function will then be
**exported**, which means callable from the DLL.

## How to import from a DLL a class / method in C++ ?

To reuse in your C++ code an already defined and compiled class /
function within a dynamic library, you will need to indicate the linker
that configuration. To do so, simply declare your class / function (in
the .h file, so the compiler knows them) but do not implement them (in
the .cpp file) and prefix its declaration with the following macro :

```c++
__declspec(dllimport)
```

This way you tell the linker that this class / function is **imported**,
which means already defined and compiled in a DLL.

## How to simply import / export a class / method in CamiTK ?

Using CamiTK, if you wish to make some classes / function exportable or
import some defined in an external DLL, you **must** include
[camitkapi.h](http://camitk.imag.fr/apidocumentation/4.1/CamiTKAPI_8h_source.html)
file and prefix all your imported / exportable classes / functions with
the following macro :

```c++
CAMITK_API
```

**How does it work ?**

CamiTK uses a standard way of creating exportable macros : All files
within this DLL are compiled with the **COMPILE_CAMITK_API** flag
defined on the command line which should not be defined on any project
that uses this DLL. This way any other project whose source files
include this file see CAMITK_API classes / functions as being imported
from a DLL, whereas this DLL sees symbols defined with this macro as
being exported.

## How to simply import / export a class / method in your CamiTK extension project?

In your CamiTK extension project, you may need to make some classes /
function exportable or import some defined in an external DLL. For
instance, you are using a component in another component or you are
building your own library and want to use it in a component. In order to
do that, you must create an header file MyComponentAPI.h or
MyLibraryAPI.h, which defines macros which make exporting from a DLL
simpler.

```c++
#if defined(_WIN32) // MSVC and mingw

    #ifdef COMPILE_MY_COMPONENT_API
        #define MY_COMPONENT_API __declspec(dllexport)
    #else
        #define MY_COMPONENT_API __declspec(dllimport)
    #endif // COMPILE_MY_COMPONENT_API

#else // for all other platforms MY_COMPONENT_API is defined to be "nothing"

        #define MY_COMPONENT_API


#endif // MSVC and mingw
```

In the header of the class that needs to be reused somewhere else, you
can add the defined symbol:

``` c++
... class MY_COMPONENT_API MyExportedClass {
...
}
```

Then, in the CMakeList.txt that compile the extension or library, you
must specify the COMPILE_MY_COMPONENT_API flag.

```bash
camitk_extension(  COMPONENT_EXTENSION
                   HEADERS_TO_INSTALL  MyComponent.h MyComponentAPI.h
                   DEFINES COMPILE_MY_COMPONENT_API
)
```

Do nothing in the CMakeList.txt that uses your class.

!!! warning
    replace "*MY_COMPONENT_API*" by something more meaningful an relevant to your class/library (if every CamiTK developer uses "*MY_COMPONENT_API*" literally, that might generates some other DLL headaches!)
