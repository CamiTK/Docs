<img src="../assets/Application.png" width="10%"></img>

Creating your own CamiTK application remain useless in 95% of the use
cases, as the already provided [CamiTK IMP application](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/CamiTK%20Tools/Camitk-imp/) is higly customizable,
feating a lots of needs users may have. Therefore, we recommend you to
check out the main IMP application, in order to see if it manages to do
what you are looking for. If you think you need to do your own
application, please [contact us through Gitlab issues](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/issues) in order to discuss it with you.
