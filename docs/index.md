# Welcome to the CamiTK wiki!

<!-- Gitlab markdown: see https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md#code-and-syntax-highlighting
-->

Here you will find information on how to develop with **CamiTK**.
As a contributor, feel free to edit any page you may want, to update it or simply creating new ones to share your knowledge. Do not hesitate to check for some advices on how to edit the wiki ! This site remains your site, and any contribution will help a lot, saving a lot of time to the **CamiTK community** !

Enjoy the [**CamiTK wiki**](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/) !