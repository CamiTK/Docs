## How to make translation of CamiTK in other language ?

The language used in implementation code by camiTK is english. but if
you want to use an another language you can use QtLinguist
(Linguist.exe) to translate all the english character strings.

What is the step to install a new language translation ?

1.  in CamitkTranslate.cmake you must insert abreviation of new language
    like \_en\_fr\_de for english, french and german.
2.  do cmake (the files translate\_en.ts, translate\_fr.ts ,
    translate\_de.ts are created or updated for all extensions).
3.  use QtLinguist with your \*.ts files to translate character string
    of \*.cpp, \*.ui, \*.h files.
4.  do cmake (the file translate\_en.qm, translate\_fr.qm ,
    translate\_de.qm are create or update for all extensions).
5.  update the listLanguageAndFlag variable in changeLanguage.cpp to
    insert for the new language his name, abreviation and flag (add
    flag\_xx.png file in png format in
    src\\sdk\\actions\\application\\resources\\flags\\16 directory)
6.  compile camitk. (then all translate\_xx.qrc file are
created).

## How to make translation of CamiTK in other language just for one extension ?

  - open the extension CMakeLists.txt and add
    **EXTRA\_TRANSLATE\_LANGUAGE** with language.

```cmake
camitk_extension(ACTION_EXTENSION
                 DEFINES COMPILE_APPLICATION_ACTION_API
                 CEP_NAME SDK
                 DESCRIPTION "Features actions used at application level (these actions are mandatory for any CamiTK application to work"
                 EXTRA_TRANSLATE_LANGUAGE de
         )
```

  - do steps 2,3,4,5,6.

## How to select your language in camiTK applications ?

1.  start camiTK imp application.
2.  in the “Help menu” do “Change Language” and choose a new language.
3.  close camiTK imp application.
4.  CamiTKx.y.z.svn.ini file are modified and you can start your camiTK
    application with this new language.
    
!!! tip
    You can find used language, abreviation and flag information are in CamiTKx.y.z.svn.ini file for example :
        * `{.text} ... language=french languageAbreviation=fr flagFilename=:/flags/flags/16/flag_fr.png ...`
