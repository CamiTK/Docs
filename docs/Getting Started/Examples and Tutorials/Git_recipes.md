This page is made to give useful command or succession of commands to perform Git tasks in the context of the [CamiTK Git workflow](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/CamiTK%20Overviews/CamiTK_Git_Branching_Model/).

Be sure to follow both:

- The [branch naming convention](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/CamiTK%20Overviews/CamiTK_Git_Branching_Model/#naming-conventions)

<!--
- The [commit message convention](./Core_developper_guidelines#Commit_messages)
-->

## Basic source Code Management using Git

### Clone the CamiTK repository and switch to develop branch

#### Clone to contribute

If you want to share your work, you first need to create an account on <https://gricad-gitlab.univ-grenoble-alpes.fr/>. Let’s say that we want to clone the CamiTK repository in a local directory named “src”.

``` bash
# R/W clone
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:CamiTK/CamiTK.git ./src

# Specify that every new push will be performed to the main repository
git config push.default current

# Now you are on the "develop" branch, ready to create your own branch
```

#### Clone to review

If you just want to check the current state of the CamiTK code, you can
clone the repository anonymously in a local directory name “src”:

``` bash
# anonymous clone
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK.git ./src
# You only have the develop branch
```

### Visualize Git work tree under Linux

Under Linux use qGit or git-cola

get qgit:

``` bash
sudo apt-get install qgit
qgit
```

If your distrib does not have the package yet, get the source code at
<https://github.com/feinstaub/qgit> .

Otherwise you can do this for git-cola :

``` bash
sudo apt-get install git-cola
git-cola
```

### Visualize the git history on the web

You can check the latest push on origin here:

- [the CamiTK develop branch    summary](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/network/develop)
- [the list of current     branches](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/branches)

### Set VIM as default message editor

``` bash
git config --global core.editor "vim"
export GIT_EDITOR=vim
```

### List all local branches and display last modified dates

From [stackoverflow](http://stackoverflow.com/questions/2514172/listing-each-branch-and-its-last-revisions-date-in-git)

``` bash
for k in `git branch | sed s/^..//`; do echo -e `git log -1 --pretty=format:"%Cgreen%ci %Cblue%cr%Creset" $k --`\\t"$k";done | sort
```

## Current development

Two types of development process are explained in this section:
- develop something new: you need to create a new feature
- fix a bug on the current development branch

The only thing that will change is how you name the local branch. Be sure to follow these [rules](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/CamiTK%20Overviews/CamiTK_Git_Branching_Model/#Naming_conventions) for naming your branch.

### Develop a new feature during the develop process

You need to create a specific local branch (see below, [Create a branch](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/Examples%20and%20Tutorials/Git_recipes/#create-a-branch-locally) and follow the rules for naming your branch: the branch should be named *feature/XXX* where XXX is a comprehensible (although rather short) description (e.g., *feature/connect-to-apple-store*).
If the branch aim is to fulfil a feature, then "closes #XXX" should be mentioned in the branch description, where `#XXX` is the issue ID. This ensure a link from your branch to the issue (and will automatically closes the issues when your branch is merged to `develop`)

### Fix a Bug during the develop process

Just proceed like for a new feature, with the difference that it should be prepend with `bug/` instead of `feature/`.

### Create a branch (locally)

As you are on the develop branch, the most usual case to work with Git is to do the job on a separate branch called feature branch creates from develop branch.

1.  Choose a proper name for your branch (see [here](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/CamiTK%20Overviews/CamiTK_Git_Branching_Model/#naming-conventions))
2.  Create the new branch and switch to the new branch:

```bash
git checkout -b feature/add-google-voice-command origin/feature/add-google-voice-command
```

You can add a detailed description to your branch with:

```bash
git branch --edit-description
```

!!! note
    that you can custom “git branch” to show your description, see [this stackover flow entry](http://stackoverflow.com/a/16710084)).

### Request a merge of (finished) feature

In the CamiTK workflow, a finished feature should be merged through a
merge request on gitlab. There are two steps to create a merge request:
1. push your local branch to origin
2. create a merge request

!!! Note
This is greatly simplified by using gitlab. TODO: update this documentation.

``` bash
# make sure you are on the correct branch
git checkout feature/occulus-viewer

# push your local branch (warning: you have to definie the push.default, see section "Clone_to_contribute" on this page
git push
# git push your branch to the main CamiTK repository (origin) and provides a http link to start a merge request
# just click on the link to open the merge request web page and submit your merge request
```

Follow up the discussion on the merge request to complete the actions required by the CamiTK team.

!!! Warning
**Do not forget to delete the local *feature/new-feature* after the merge is accepted (see below)**

### Delete a branch after the merge-request was accepted

To delete a specific branch (e.g., `bug/compilation-on-minitel`):

``` bash
git branch -d bug/compilation-on-minitel
```

!!! Note
    if you did not commit all your modification, there will be a message (you can then decide if you want to remove all and force the deletion using the `-D` option instead).

To delete all local branch that has been merged on origin:

``` bash
git branch --merged | egrep -v "(^\*|master|dev)" | xargs git branch -d
```

### Working on a feature branch

To version your code, do not hesitate to commit to your branch as often as possible (and to push it so that your changes can appear in the merge request for review or sharing).
When you push, the gitlab-ci system is automatically started unless you add `[ci skip]` or `[skip ci]` to the commit message.


#### Updating the git local index (staging new content)

You don’t need to perform an “add” command if the only thing you have done is to modify an already versionned file(s). To add a new single document you created:

``` bash
git add myDocument.cpp
```

To add a set of documents created in the current directory. For example, let’s say that you have created a file called “myFile.cpp” plus a dir called “testDir” and files in it. This command will stage all the new files and dir in the current dir.

``` bash
git add .
```

You can replace the “.” with a relative or absolute path.

#### Committing changes and adds

To commit all these changes loccaly, we perform a commit:

``` bash
git commit -m "meaningful commit message"
```

Please, be sure to read this [great documentation](https://chris.beams.io/posts/git-commit/) on how writting a good commit message.


#### Pushing the changes to origin (gitlab server)

When you push, the gitlab-ci system is automatically started unless you add `[ci skip]` or `[skip ci]` to the commit message.
``` bash
git push
```

#### Updating a feature branch that have conflicts with current develop

Sometimes your feature branch will be to distant to `develop`. In this case, gitlab will complain and say that you can not merge 
your feature branch in `develop`.
In this case **DON'T FOLLOW** the instructions given by gitlab. It is better to do the following steps :

```bash
# First update your local develop branch
git checkout develop
# Get the current remote version 
git fetch -p origin
# Merge the changes from `origin/develop` into your local `develop` branch. Your local `develop` branch will then be in sync with the remote repository
git merge origin/develop
# Check out your feature branch
git checkout feature/new-feature
# Merge the updated `develop` into your feature branch to include the latest changes
git merge develop
```

Now you can solve the conflicts. Note for Linux user, we recommand using a GUI merge tool to help to solve the conflicts, for instance
`git mergetool --tool=kdiff3`
This only updates the local feature branch. Then, you need to:

- check all the files (some automatic merges might *not be what you want*)
- if needed, add and commit your changes (e.g. `git add .; git commit -m "Solved merge conflicts"`)
 
Push you branch to remote:
```
git push origin feature/new-feature
```

Gitlab should start the CI pipeline and remove the merge conflict warnings.

## Release

### Create a specific release branch

We work only on the shaping of the release, only bug corrections, no new feature.

``` bash
git checkout develop
git branch release/4.0.0
```

### Release a new CamiTK version

Once the release is ready, follow the three following steps.
1) push release branch on origin code

``` bash
git push
```

2) Create a merge request on **master**.
Use the gitlab web interface to start a new merge request on **master** (be careful to **select the master branch as the target to merge into**!)

3) Create a tag corresponding to the release number.
When the merge is accepted, do not forget to create a new tag with the release number directly on the gitlab web interface (e.g. “4.0.0”)

4) Create a merge request on **develop**.
Use the gitlab web interface to start a new merge request on **develop** in order to push the modifications back to develop branch **Do not forgot this step** as bug
fixes done in the release branch should be pushed on **develop** too!

5) Increment the CamiTK version number in the top level CMakeLists.txt

6) Delete the release branch on origin as well as locally

``` bash
git branch -d release/4.0.0
git push origin --delete release/4.0.0
```

### Fix a Bug during the release process

You are in the release process on the release branch and your are correcting a bug: just follow the [CamiTK Branching - Naming convention](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/CamiTK%20Overviews/CamiTK_Git_Branching_Model/#naming-conventions) as described by the picture in [depot referentiel](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/CamiTK%20Overviews/CamiTK_Git_Branching_Model/#working-with-depot).

If you fixed an important bug that you want to commit immediately to the develop branch for some reasons, follow the steps below to make your corrections available in the develop branch without waiting the end of release process.

Verify that all work is commited on the release branch. If everything is ok do the following:

1.  push your local branch to origin
2.  create a merge request on develop

Follow up the discussion on the merge request to complete the actions required by the CamiTK team.

But, **don’t delete the branch** after the merge request was done.

### Try a release

If a release is getting prepared on origin (that is if you are working with some other developer on a specific release and this developer already pushed some changes on the release branch on origin), this is how you get a local copy to try it on:

``` bash
git pull
git checkout -b release/4.0.0 origin/release/4.0.0
```

## Hotfix

### Create a specific release branch

From the master branch create a new hotfix branch by respecting the [branch naming convention](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/CamiTK%20Overviews/CamiTK_Git_Branching_Model/#naming-conventions).

``` bash
git checkout -b hotfix-4.0.1 master # Switched to a new branch "hotfix-4.0.1"
```

### Release a new CamiTK hotfix version

Make your strategic corrections, committing your modifications as you go.

Once the release is ready, follow the three following steps.

1) Release code on master branch and add tag:

    1.1. push your local branch to origin
    1.2. create a merge request on **master**
    1.3. Follow up the discussion on the merge request to complete the
    actions required by the CamiTK team.
    1.4 Create a tag directly using Gitlab interface

**don’t delete the branch** after the merge request was done.

2) Put modifications back to develop branch from the new hotfix branch:

    2.1. push your local branch to origin
    2.2. create a merge request on **develop**
    2.3. Follow up the discussion on the merge request to complete the actions required by the CamiTK team.

**delete the branch** after the merge request was done.

### Try a hotfix

If a hotfix needs to be verified/tested/modified by more than one developer (that is one need more than one developer to work on a specific hotfix), the main hotfix developer should first push the hotfix branch on origin:

``` bash
git push origin hotfix-4.0.1
```

**Don’t create a merge request**

Once the hotfix branch is on origin, this is how to get a local copy to verify/test/modify it:

``` bash
git pull
git checkout -b hotfix-4.0.1 origin/hotfix-4.0.1
```

## Git snippets

This is a small collection of little recipes for current git operations.

### Undo/Cancel the last commit

Simply use this command to completely undo the last commit and erase history.

``` bash
git reset --hard HEAD~1
```

!!! Note
    `~` replaces the minus (`-`) sign, so this means reset to `HEAD - 1` commit.

You can also simply use this command to undo the last commit but leave the history (and your files):

``` bash
git reset --soft HEAD~1
```

### Undo (uncommitted) changes

For instance, you have accidentally removed a file. To revert changes to modified
files:

``` bash
# Becareful: It resets the index and working tree. Any changes to tracked files in the working tree since the last commit are discarded.
git reset --hard
```

This is the same as `git reset –hard HEAD`.

### Clean the current working tree

Use with care

``` bash
# check first what is going to happen
git clean -nd
# if ok, do it, f=force, d=remove directories
git clean -fd
```

### Rename a branch

`git branch -m compile-on-sid <newName>`

### Diff between two branches

There are a lot of different ways to compare branches:

``` bash
git diff b1..b2
git diff b1...b2
git log b1..b2
git shortlog b1..b2
```

To get only the filenames that have changed:

``` bash
git diff --name-status b1..b2
```

See [this stackoverflow answers](http://stackoverflow.com/questions/822811/showing-which-files-have-changed-between-two-revisions)

### Solve a conflict after a pull

Just type:

``` bash
git mergetool
```

Running `git mergetool` will prompt the user for each conflict, and propose a default application. On Linux, we recommend to use kdiff3 as the git diff tool.

### Copy commits from one branch to the current branch

Just type:

``` bash
git cherry-pick commit-hash
```

to apply the commit *commit-hash* (made on another branch) to your current branch.

It is also possible to copy a range of commits from one branch to the current branch.

### List the files modified by the last commit

Just type:

``` bash
git log -n 1 --name-only --pretty=
```
