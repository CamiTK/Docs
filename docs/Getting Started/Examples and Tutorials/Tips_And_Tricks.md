## Easily edit environment variables on Windows system

Environment variables are used on all system (Windows, Linux, Mac OS ...) to define specific values used by software or the system itself.

On Windows system, editing environment variables can be really hassles if you use the default system tool. To answer this issue, we recommend you to use the freeware [Rapid Environment Editor](http://www.rapidee.com/download/RapidEE_setup.exe ).

When launching it, on the left side of the application are displayed the system environment variables.

You may want to edit them using this explorer.

!!! warning
    You will need administrator privilege to edit the system environment variable. Otherwise, you will be limited to local environment variables (located on the right side of the application).

!!! note
    The specific environment variable `Path` is the most important one. It contains a list of folders in which the system will look for finding an application or a specific library.
    The Path environment variable is thus really important to be correctly set, in order for CamiTK applications to find their dependencies (ITK, VTK libraries ...).


## Use Dependency Walker to understand why the application can not load plugins on Windows system

[Dependency Walker](http://www.dependencywalker.com) is a free utility that builds a hierarchical tree diagram of all dependent modules. Dependency Walker is very useful if a module can not be loaded by your application. If you load the module into Dependendy Walker, it gives you the list of all the functions that are exported by that module, and which of those functions are actually being called by other modules. It will detect if the problem comes from  missing modules, invalid modules, import/export mismatches, circular dependency errors, mismatched machine types of modules, or module initialization failures.


## Use RSA public/private keys for your SSH transaction

###Generate private and public keys

Here you will find information to generate a couple of SSH public and private key using the [RSA](http://en.wikipedia.org/wiki/RSA_%28algorithm%29) algorithm. These keys are used during SSH transaction for authentication. Once correctly configured and loaded in your system session you ssh transactions from your computer and a server will no longer prompt for a password.

The public key is shared among the different servers you wish to connect and the private key is stored on your computer.

!!! warning
    The private key is really important and you may not share it! Store it wisely.

#### on Windows

To create private / public RSA keys, please use [PuttyGen](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html ) wich is an RSA key generator for windows.
* Install and run PuttyGen
* Generate a private / public RSA keys pairs.

![RSA_Install](./assets/Install_camitk_windows_1.png)

Don't forget to move your mouse to randomize the RSA key generation !

Note that the program uses mouse movement to generate randomness within the key.
* Add a passphrase to this key for more security

![RSA_Install2](./assets/Install_camitk_windows_2.png)
Use a passphrase for more security before saving your public / private keys

!!! note
    A passphrase is the private key password. It is `highly` recommended to use one. Else, if someone find your private key, he can pretend to be you on any ssh transaction.

* Save the public and private key on your disk.

!!! note
    On Windows you generate public and private keys under the `.ppk` format, which is a putty specific format.
    On Linux, you may generate a couple of keys in another format. PuttyGen allows you also to import a Linux couple of SSH keys and save them back into its own format.

* Store your private key in a secure place (such as your own usb key).

#### on Linux
To create private / public RSA keys on Linux, you may use the `ssh-keygen` command.

* Run `ssh-keygen`
* Provide a filename to store both private and public generated keys
* Add a passphrase to this key for more security

### Store your public key on a server
In order for the SSH transaction to work with your couple of public / private keys you will need to store your public key on the server you wish to establish an SSH transaction with.

#### on the Gricad Gitlab page
Click on `settings` on your profile menu and `SSH Keys` on the left panel.

* Copy your public key file content in the text box.

!!! note
    The syntax of the key is:
        `ssh-rsa` `characters of the public key`

!!! warning
    Watch out for adding `blank` (whitespace) or `carrier return` characters in the content of your public keys.
    The content of the file following `id_rsa` does not contain any carrier return character, the RSA characters of the keys are on only `ONE` line.
    ![public_key](./assets/Public_key.png)

#### on any Linux running server
On any other server running under a Linux operating system (which is fortunately the most common case), here are the steps to follow.

!!! note
    The svn-timc.imag.fr server, holding restricted timc-imag source code is concerned by those steps.

At this point we consider you have generated a couple of RSA public and private key on your system.

You will need to copy your private key onto the server and add its content to the `~/.ssh/authorized_keys` file.
To simply do it, we will use FileZilla software.

* Download and install [FileZilla](https://filezilla-project.org/download.php?type=client).

!!! tip
    On a Debian Linux distribution you may directly install FileZilla by typing:

    ```bash
    sudo apt-get install filezilla
    ```

* Run FileZilla. Indicate the `server name`, your `login`, your `password` and for the port, indicate `22` in the menu bar.

![filezilla_login](./assets/Filezilla_login.png)
Example for the `svn-timc.imag.fr`

On the left side of the application is files explorer on your computer and on the right side on the server you just connected. On the Linux server, the right side location points to your home directory (~).
* If it does not exist, create the `~/.ssh` directory, else open its content.
* If it does not exist, create the file `~/.ssh/authorized_keys`.

!!! note
    Authorized_keys file contains all the public keys stored on this computer and your login for SSH transaction.
    You will need to copy your public key content into this file.

* Download the authorized_keys file, modify it to add your public key content and upload it back to the server.

#### Store your private key in your session
In order to use your private key, you need to load it into your current running session. Here are the steps to do it, depending on your operating system:

**on Windows**
* Run `pageant` tool, by default installed with Putty softwares. If it is not installed, you may [download](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) and run it.

!!! note
    Pageant is launched in background. An icon appeared on the bottom menu bar, close to the clock.

* Right-click on the pageant icon and select `add key`.

![Peagent](./assets/Peagent.png)
Select `add key`

* Choose your private key on the .ppk format.
* Type your passphrase as pageant asked for it.

**on Linux**
* Store your private key in `~/.ssh/id_rsa` (create the folder if necessary)
* Run the `ssh-add` command to load your private key into the session.

## How to check if a DLL is compiled for 32 bits or 64 bits on Windows system ?

### Using Visual Studio compiler
* Open visual studio command prompt
* Use this command: `dumpbin /headers C:\path\name.dll`
* at the very beginning you will find “FILE HEADER VALUES”, the first value will have a “(x86) ” for 32 bit and “(x64)” for 64 bit

### Using MinGW
* Download and install [CyGWin](http://cygwin.com/index.html).
* Launch a console in your windows environment.
* Use the following command to check the build-type of your DLL
```bash
file yourLibrary.dll
```

* Check the output:
** PE32 stands for 32bits DLLs
** PE32+ stands for 64bits DLLs



## How to display point and cell data in camitk-imp

To test the expected behaviour, lets use the "plate-with-data.vtk" example file that came with the feature (it originally comes from the VTK data test suite and contains well scaled vectors relatively to the data).

Display the &quot;Data&quot; tab in the Property Explorer, it should list all the loaded data attached to the point, the cell or global mesh. Data can be either scalars, 3D vectors or 9D tensors.

3D representation of scalar and 9D tensors can be activated by clicking on the checkbox on the left corresponding to the data.

3D Representation of vector data can be done simultaneously, with a 3D vector (arrows, unscaled arrows or hedge hog representation) and as a colour corresponding either to magnitude (norm), 1st, 2nd or 3rd component of the vector. The colour representation can only be one of these four choices.

Choosing a specific vector data representation, use the two combo boxes at the bottom of the &quot;Data&quot; tab:

- the first one on the left let you choose the representation of your vector data (it does not do anything for scalar or tensor data)
- the second one on the left let you choose specific ways of rendering 3D vectors (arrows, unscaled arrows which does not use the data magnitude to scale the representation and hedge hog which uses simple lines instead of arrows).

Therefore:

- To show the vector data as 3D vectors, set the first combo box on the left to &quot;3D Vector&quot;, select the 3D vector representation to either arrows, unscaled arrows or hedge hog and tick the checkbox on the vector data of your choice.
- To show the vector data as colours, set the first combo box to either norm, 1st, 2nd or 3rd component and then tick the checkbox on the vector data of your choice.

Changing the first combo box will let you see which representation is active (the checkbox is ticked for the corresponding active representation).
Only one colour representation is possible at a time, but both one colour representation and 3D vectors are possible together.

The vector data can be set either on the cell field or on the point field:

- if set on the cell field, then the arrows are centred on the cell and the cell is coloured with the corresponding colour (no interpolation between the points)
- if set on the point field, then the arrows starts at the vertex and the color is attributed to the vertex (resulting in an interpolation along the cells' surface)



## Configure Visual Studio 2010 C++ express to compile in 64bits

Visual Studio 2010 express edition might compile in 64bits since you have installed the 7.1 version of the Windows SDK.
* Please install the [Windows SDK (7.1) for Windows 7](http://msdn.microsoft.com/en-us/windows/bb980924.aspx)

!!! note
    If you installed (you or windows update) the SDK 7.1 `SP1` please install this [ patch for Windows 7.1 SDK x64 compiler](http://www.microsoft.com/en-us/download/details.aspx?id=4422) as this service pack broke up the x64 compiler.

!!! warning
    If you deal with a problem for the installation of Windows SDK 7.1 (which have to be installed before VS2010 SP1) visit this [link](http://www.mathworks.fr/support/solutions/en/data/1-FPLDGV/).

* Change your Visual Studio projects configuration:
    * Go to Properties of one of your projects.
    * On the top of the dialog box there will be a `Configuration` drop-down menu. Make sure that selects `All Configurations`.
    * On the right there is a `Configuration Manager` button - press it.
    * In the dialog that comes up, find your project, hit the Platform drop-down, select New, then select x64.

!!! note
    When you return to the Properties dialog box, the "Platform" drop-down should now read `x64`.

* Finally, change your toolset. In the Properties menu of your project, under Configuration Properties | General, change Platform Toolset from `v100` to `Windows7.1SDK`.