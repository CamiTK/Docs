This page presents various bash scripts that can be integrated into IDE or Git GUI manager (such as `qgit`) in order to quickly perform Git tasks in the context of the [CamiTK Git workflow](./CamiTK_Git_Branching_Model).

In order to work properly these scripts require:

- that you are connected on the gitlab server (and have the proper credentials)
- `git`
- `kdialog` command (required for most of the scripts, it enhances the UI, it is provided on Debian by package `kde-baseapps-bin`)
- `curl` (for triggering the pipeline through the REST gitlab API)

Be sure to have read both:

- The [branch naming convention](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/CamiTK%20Overviews/CamiTK_Git_Branching_Model/)

<!--
- The [commit message convention](./Core_developper_guidelines)
-->

## Create a New Branch

``` bash
#!/bin/bash
#
# qgit &Create New Branch
#

newBranchName=$(kdialog --title "Create a new branch" --inputbox "What is the new branch name?<br/><br/><b>Note:</b> CamiTK branch naming convention examples:<ul>
<li><tt><i>feature/short-description</i></tt> for other features</li>
<li><tt><i>bug/short-description</i></tt> for other bugs</li>
</ul>
For hotfix or release, please use command line.<br/>
See the <a href='https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/dev/CamiTK_Git_Branching_Model/#naming-conventions>CamiTK wiki documentation page about branch name convention</a>" "feature/")

if [ "$?" = 0 ]; then
	git checkout -b $newBranchName
else
	echo "Creating new branch cancelled."
fi;
```

## Checkout an existing branch

```bash
#!/bin/bash
#
# qgit name "Checkout &Branch"
#

branches=$(for branch in $(git for-each-ref --format='%(refname)' refs/heads/); do echo ${branch/refs\/heads\//}; done)
output=$(kdialog --title "Checkout branch" --combobox "Choose the branch" $branches)

if [ "$?" = 0 ]; then
	git checkout $output
else
	echo "Checkout cancelled."
fi;
```

## Delete a branch

```bash
#!/bin/bash
#
# qgit name "&Delete Branch"
#

# get the list
branches=$(for branch in $(git for-each-ref --format='%(refname)' refs/heads/); do echo ${branch/refs\/heads\//}; done)
branch=$(kdialog --title "Delete branch" --combobox "Choose the branch to delete" $branches)

kdialog --warningcontinuecancel "Are you sure you REALLY want to delete $branch?";
if [ "$?" = 0 ]; then
	git branch -d $branch
	echo "Branch deleted."
else
	#	kdialog --sorry "Operation canceled";
	echo "Deletion cancelled."
fi;

```

## Synchronize with origin

This script will make sure the `develop` branch is up to date and will delete local branches that have been merged in origin.

```bash
#!/bin/bash
#
# qgit name "Synchronize with &Origin"
#

kdialog --msgbox "Checkout develop, pull and cleanup branches"

git checkout develop

git pull

kdialog --warningcontinuecancel "Do you want to delete local branches that have been merged already in origin?"

if [ "$?" = 0 ]
then
	git branch --merged | egrep -v "(^\*|master|dev)" | xargs git branch -d
	echo "Merged branches deleted."
else
	echo "Merged branch not deleted."
fi
```


## Push and Create a new Merge Request

```bash
#!/bin/bash
#
# qgit name "&Push and Create Merge Request"

# name of the current branch
currentBranch=$(git rev-parse --abbrev-ref HEAD)

# push
# As git push prints to stderr, redirect stderr to stdout
pushmsg=$(git push --set-upstream origin $currentBranch 2>&1)

# setup upstream for local branch (if someone else modify the current
# branch, then "git pull" will be configured to get this modification
git branch --set-upstream-to=origin/$currentBranch $currentBranch

url=$(echo "$pushmsg" | grep https | cut -c11-)

echo $pushmsg
echo
echo "URL is $url"

kdialog --yesno "Branch pushed.<br/>Create/update the merge request on gitlab?"

if [ "$?" = 0 ]
then
	curl --request POST $url
	echo "Merge request created."
else
	echo "To create merge request later, use URL:"
	echo "$url"
fi

kdialog --yesno "Visit the merge request on gitlab?"
if [ "$?" = 0 ]
then
	firefox $url
	echo "Merge request created."
else
	echo "To create merge request later, use URL:"
	echo "$url"
fi
```

## Trigger CI Pipeline

You need to fill in your personal token for this script to work.

```bash
#!/bin/bash
#
# qgit name "&Trigger CI Pipeline"
#

projectId="514"

token="YOUR-TOKEN-HERE"

currentBranch=$(git rev-parse --abbrev-ref HEAD)

stages="CHECK CONFIGURE BUILD TEST COVERAGE"
ciStage=$(kdialog --title "Trigger CI Pipeline" --combobox "Choose the level of CI to perform:<ul>
<li><tt>CHECK</tt> check the pipeline is working</li>
<li><tt>CONFIGURE</tt> check and configue stage</li>
<li><tt>BUILD</tt> all above and build stage</li>
<li><tt>TEST</tt> all above and test stage</li>
<li><tt>COVERAGE</tt> all above and coverage stage</li>
</ul>" $stages)

if [ "$?" = 0 ]; then
	case "$ciStage" in
	CHECK)
	  camitkStage="10"
	  ;;
	CONFIGURE)
	  camitkStage="20"
	  ;;
	BUILD)
	  camitkStage="30"
	  ;;
	TEST)
	  camitkStage="40"
	  ;;
	COVERAGE)
	  camitkStage="50"
	  ;;
	*)
	  camitkStage="0"
	  ;;
	esac

	curl --request POST \
        -F "variables[CAMITK_CI_MODE]=Experimental" \
	    -F "variables[CAMITK_CI_STAGE]=$camitkStage" \
	    -F token=$token \
	    -F ref=$currentBranch \
	    https://gricad-gitlab.univ-grenoble-alpes.fr/api/v4/projects/$projectId/trigger/pipeline
else
	echo "Trigger CI Pipeline cancelled."
fi;




```

## Trigger the Debian Package Pipeline

You need to fill in your personal token for this script to work.

```bash
#!/bin/bash
#
# qgit name "Trigger &Debian Package Pipeline"
#

projectId="514"

token="YOUR-TOKEN-HERE"

currentBranch=$(git rev-parse --abbrev-ref HEAD)

kdialog --yesno "Do you <b>really</b> want to trigger the Debian packaging pipeline?"

if [ "$?" = 0 ]
then
    curl --request POST \
        -F "variables[CAMITK_CI_MODE]=Experimental" \
        -F "variables[CAMITK_PACKAGING]=TRUE"\
        -F "variables[CAMITK_CI_STAGE]=70" \
        -F token=$token \
        -F ref=$currentBranch \
        https://gricad-gitlab.univ-grenoble-alpes.fr/api/v4/projects/$projectId/trigger/pipeline
else
        echo "Trigger Debian Package Pipeline cancelled."
fi
```



## Other: Merge Branch to origin/develop

```bash
#!/bin/bash
#
# qgit name: "&Merge Branch to origin/develop"
#

# Step #1 update local develop branch
git checkout develop
git pull origin develop
 
# Step #2 rebase your feature to the upstream develop branch
branches=$(for branch in $(git for-each-ref --format='%(refname)' refs/heads/); do echo ${branch/refs\/heads\//}; done)
branch=$(kdialog --title "Merge branch" --combobox "Choose the branch to merge" $branches)

git checkout $branch
git rebase develop
 
# Step #3 go back to the develop branch...
git checkout develop
 
# Step #4 ...to merge your work
kdialog --warningcontinuecancel "Are you sure you REALLY want to merge branch <tt>$branch</tt> into develop?";
if [ "$?" = 0 ]; then
	git merge --no-ff $branch
	git push origin develop
	echo "Merge completed."
	kdialog --warningcontinuecancel "Do you want to delete <tt>$branch</tt> now?";
	if [ "$?" = 0 ]; then
		git branch -d $branch
		echo "Branch deleted."
	fi;
else
	#	kdialog --sorry "Operation canceled";
	echo "Merge cancelled."
fi;
```