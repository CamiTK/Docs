You might need to add some Qt Widget to your CamiTK action. You can
choose to

  - add parameters as shown [this page](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/Examples%20and%20Tutorials/How_to_add_Qt_Widgets_as_additional_action_parameters_for_actions_created_using_the_CamiTK_wizard/)
  - add your own Qt widgets created using the Qt designer as shown [this page](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/Examples%20and%20Tutorials/How_to_add_Qt_Widgets_as_additional_action_parameters_for_actions_created_using_the_CamiTK_wizard/#Learning_by_doing)
  - mix the default CamiTK action widget with your own widget. That’s
    what you will learn with this topic.

## How is built the default action widget ?

The default action widget is a QWidget . This widget contains a QLayout.
This layout contains three objects:

  - The first object (indice 0) is the name of the action.
  - The second object (indice 1) contains the panel containing the
    action’s properties (“Description”, “target” and “parameters”).
  - The third object (indice 2) is the panel containing the two buttons
    “Apply” and “Revert”.

You can insert your own widget in this layout at one of the previous
indice.

## Create your widget

You can create your widget using the Qt Designer or simply by using Qt
objects. For example, create a new QPushButton:

```c++
QPushButton *yourButton = new QPushButton("The text of your button");
QObject::connect(saveButton, SIGNAL(released()), this, SLOT(your_callback()));
```

## Add your widget in the default action widget

And then add your widget in the default widget, ie add your widget in
the default action widget layout:

```c++
 QLayout *informationFrameLayout = Action::getWidget()->layout();
 informationFrameLayout->addWidget(yourButton); // add the widget at the end of the layout
 Action::getWidget()->setLayout(informationFrameLayout);
```

You can also declare the action widget as a member of your action class,
and instanciate it as the default action widget:

```c++
myWidget = new ActionWidget(this);
```

And then you get the layout of the widget to insert your own widget in
it:

```c++
QBoxLayout * informationFrameLayout = dynamic_cast<QBoxLayout*>(myWidget->layout());
informationFrameLayout->insertWidget(0, yourButton); // insert the widget at the given index
```

You might also need to hide the default buttons
:

```c++
ActionWidget *actionWidget = dynamic_cast<ActionWidget * >(this->actionWidget);
actionWidget->setButtonVisibility(false);
```
