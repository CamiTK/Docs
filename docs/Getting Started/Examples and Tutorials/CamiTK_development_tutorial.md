#CamiTK Development Tutorial

The following set of tutorials will show you how you could use CamiTK for development.

If you have any problems/doubts/comments regarding the following tutorials, please do not hesitate to [ask for help](../../CamiTK%20Overviews/How_To_Ask_For_Help).

<!--
| Wizard | Developing | Testing |
| ---    | ---------- | ------- |
| <ul><li>How to create a component</li><li>How to create an action</li><ul><li>How to add a VTK Widget to an action</li><li>How to add your own Qt Widgets to your action</li><li>How to display new object in a CamiTK action</li><li>How to add Qt Widgets as additional action parameters for actions</li><li>created using the CamiTK wizard</li></ul></ul> | <ul><li>CamiTK extensions code examples</li><li>How to export/import DLL into CamiTK</li><li>How to create an application</li><li>Check memory leaks</li><li>Step by step creation of a segmentation tool</li></ul> | <ul><li>Testing in CamiTK</li></ul> |
-->

## Tutorial source

The tutorial CEP is provided with the CamiTK Community Edition. It contains some examples that could help you develop your own extension:

| Type | Name | What does it demonstrates? | Source Code |
| ---- | ---- | -------------------------- | ----------- |
| Action | averagevoxelvalues | (image) Compute the average voxels value in the neiborhoods of the currently selected voxel | `tutorials/actions/averagevoxelvalues` |
| Action | basicpicking | (image and mesh) A basic picking demo extension. It contains two actions demonstrating one way to get information on mesh or images using picking (i.e., clicking on the object in the 2D/3D interactive viewer) | `tutorials/actions/basicpicking` |
| Action | hardcodedpipeline | (misc) Demo how to apply/run two actions from another one using a hard-coded pipelines | `tutorials/actions/hardcodedpipeline` |
| Action | meshanimation | (mesh) Demo how to "animate" a mesh (i.e., dynamically apply displacements to a mesh). Shows three ways of doing this: basic animation, with a timer and using an external library (see the `shakerlib` library)  |  `tutorials/actions/meshanimation` |
| Action | meshpointdata | (mesh) Demo how to (programmatically) show/hide mesh point data using a color scale  |  `tutorials/actions/meshpointdata` |
| Action | properties | (misc) Demo how to create various type of action parameters and manipulates component dynamic properties (for instance add some dynamic properties in an action) |  `tutorials/actions/properties` |
| Action | selection | (mesh) Demo how to work with the mesh selection |  `tutorials/actions/selection` | 
| Action | sleepingwhileworking |(viewer and image) Demo how to directly manipulate the viewers to change the camera and selected slice in an 3D image (and pretend you are busily doing it yourself!) |  `tutorials/actions/` |
| Action | vtkwidget | Demo how to add a VTK 3D widget in a viewer (a VTK widget is a type of interactor directly available in a 3D VTK renderer window). See also [the advanced topic "Add VTKWidget to an action in CamiTK"](../../Advanced%20Topics/Add_VTKWidget_to_an_action_in_CamiTK/) |  `tutorials/actions/vtkwidget` |
| Component | mixed | Demo how to integrate an image (`.mha`) and a mesh (`.vtk`) together in a same top-level component | `tutorials/components/mixed` |
| Component | pickme | (mesh) Demo how to use `pointPicked(..)` and `cellPicked(..)` methods to interact directly with a component | `tutorials/components/pickme` | 
| Component | abort | Demo how CamiTK handle `AbortException` in component constructor. | `tutorials/components/abort` | 
| Viewer | `MyViewer` | Demo how to create new type of CamiTK viewer and integrate it in a CamiTK Application. The viewer is a  really simple text viewer (it just send a text message everytime a component is read/close and gives information about current number of components) | `viewers` |
| Application | basic | A simple (4 lines of code!) CamiTK application that consists only of a central axial viewer | `applications/basic` |
| Application | fancy | A fancy main window that have a different background and different viewer placement | `applications/fancy` |
| Application | menubar | A simple application with a main window that has a three items menu bar: a file menu (open, close, quit), an action menu (present the available action for the currently selected component), and a help menu (with a classic "about"). The most difficult part is to manage the "Action" menu (as it depends on the  selection). The main window has also two viewers (component explorer and 3D viewer) | `applications/menubar` |
| Application | objectcontroller | Shows a simple object controller GUI (gives an idea of what is possible to do in CamiTK thanks to qtpropertybrowser) | `applications/objectcontroller` |
| Application | simple | A simple application that has no menubar, no toolbar and no statusbar. It demonstrates that an application can be a very classical 3D view and/or 4 viewers, with just a MedicalViewer as the main widget (note the default black background set to the 3D viewer) | `applications/simple` |
| Application | testlogger | An application that test all the CamiTK logger methods | `applications/testlogger` |
| Application | testloggercrash | An application that test all the CamiTK logger and its behaviour when somethings goes horribly wrong | `applications/testloggercrash` |

<!-- | Action | pythonscripting | |  `tutorials/actions/pythonscripting` | -->

<!-- | Application | nogui | [not] | `applications/nogui` | -->


## Other tutorials

This wiki contains additional how-tos:

- [how to add Qt Widgets as additional action parameters for actions created using the CamiTK wizard](../How_to_add_Qt_Widgets_as_additional_action_parameters_for_actions_created_using_the_CamiTK_wizard)
- [How to add your own Qt Widgets to your action](../How_to_add_your_own_Qt_Widgets_to_your_action)
- [A step by step guid to create a segmentation tool that lets you manually segment an image by drawing along the contours of the objects of interest, similarly to ITK-SNAP](../Create_a_manual_segmentation_tool)


