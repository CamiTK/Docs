# Setup CamiTK 4 Development Environment on Ubuntu LTS (Xenial 16.04 or Bionic 18.04)

In order to develop on Ubuntu LTS (16.04 or 18.04), you will need to:

- install required dependencies and build tools
- build and install CamiTK 4 stable
- install development tools to work on your CEP.

!!! Note
    that this should work for any Ubuntu version older than 16.04.

## Install dependencies and build tools

This is quite easy as everything is available as packages.

Just type:
```bash
sudo apt update
sudo apt install devscripts equivs
export CAMITK_LATEST="4.1.2"
workingDir=$(mktemp --tmpdir -d camitk-$CAMITK_LATEST-build-tmp.XXX)
cd $workingDir
wget https://salsa.debian.org/med-team/camitk/raw/debian/$CAMITK_LATEST-1/debian/control
sed -i "s/10)/9)/g" control
sudo apt update
sudo mk-build-deps -t "apt -y --no-install-recommends -o Debug::pkgProblemResolver=yes" --install ./control
```

## Build and install CamiTK 4 stable

```bash
cd $workingDir
wget https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/-/archive/$CAMITK_LATEST/CamiTK-$CAMITK_LATEST.tar.gz
tar xf CamiTK-$CAMITK_LATEST.tar.gz
mkdir build
cd build
cmake ../CamiTK-$CAMITK_LATEST -DCEP_IMAGING=TRUE -DCEP_MODELING=TRUE -DCEP_TUTORIALS=TRUE -DCMAKE_INSTALL_PREFIX="/usr/local"
make # To build in parallel: add -jX where X is your number of CPU
sudo make camitk-ce-global-install
```

## Install development tools
The CamiTK team recommands `kdevelop` IDE.

Install kdevelop and git:

```bash
sudo apt-get install kdevelop git
```


## Check the installation

Type:

```bash
camitk-config --config
```

You should get details information about camitk version, where camitk is looking for extensions, and the default installed extensions.
You're ready to run `camitk-wizard`!
