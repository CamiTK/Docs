# Setup CamiTK bleeding edge Development Environment on Debian Stable, Ubuntu 17.04 or later

Start from a fresh install of `debian-stable`, `Ubuntu 17.04` or later version of Ubuntu, you will need to:
- install the the dependencies and CamiTK from the `develop` branch source code
- Install the recommended development tools

## Install CamiTK development version from the `develop` branch

### Dependencies

To install CamiTK dependencies, just type:

```bash
sudo apt-get build-dep camitk
```

!!! Note
    if you get the following error message

    `E: You must put some 'source' URIs in your sources.list`
    
    you need to add source repository, see the [debian documentation](https://wiki.debian.org/SourcesList)


### Clone and build CamiTK from the `develop` branch

```bash
git clone -b develop --single-branch https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK.git
mkdir build-CamiTK
cd build-CamiTK
cmake ../CamiTK -DCEP_IMAGING=TRUE -DCEP_MODELING=TRUE -DCEP_TUTORIALS=TRUE -DCMAKE_INSTALL_PREFIX="/usr/local"
make
sudo make sdk_global_install
```

Use `git pull` to update this version everytime a new version is available on the `develop` branch.


## Development tools
The CamiTK team recommands `kdevelop` IDE.

Install kdevelop and git:

```bash
sudo apt-get install kdevelop git
```


## Check the installation

Type:

```bash
camitk-config --config
```

You should get details information about camitk version, where camitk is looking for extensions, and the default installed extensions.
You're ready to run `camitk-wizard`!
