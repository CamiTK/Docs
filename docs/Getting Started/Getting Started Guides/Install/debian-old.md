# Setup CamiTK 4 Development Environment on Debian Old Stable (jessie)

## Install dependencies and build tools

There are some required software needed to be able to compile CamiTK.
Fortunately they are all available as packages.
Just do
```bash
sudo apt-get install build-essential devscripts equivs fakeroot quilt dh-make automake libdistro-info-perl less vim piuparts git xpra net-tools libconfig-model-dpkg-perl libconfig-model-itself-perl devscripts cdbs git-buildpackage autopkgtest schroot gdb iwyu python-yaml lcov gcovr libxt-dev cmake cmake-gui qtbase5-dev libqt5xmlpatterns5-dev libqt5opengl5-dev qttools5-dev qttools5-dev-tools libqt5webkit5-dev xsdcxx libxerces-c-dev wget xvfb xauth
```


## Build and install newer version of VTK, gdcm and ITK

This can also be done easily:

### VTK 6.3.0
```bash
cd /tmp
mkdir dependencies
cd dependencies
wget http://www.vtk.org/files/release/6.3/VTK-6.3.0.tar.gz
tar xf VTK-6.3.0.tar.gz
mkdir build-VTK
cd build-VTK
cmake ../VTK-6.3.0 -DBUILD_DOCUMENTATION=FALSE -DBUILD_EXAMPLES=FALSE -DBUILD_SHARED_LIBS=TRUE -DBUILD_TESTING=FALSE -DVTK_Group_Qt=TRUE -DVTK_QT_VERSION=5 -DModule_vtkGUISupportQt=TRUE -DModule_vtkGUISupportQtOpenGL=TRUE -DModule_vtkGUISupportQtSQL=TRUE -DModule_vtkgl2ps=TRUE -DCMAKE_INSTALL_PREFIX="/usr/local" -DVTK_RENDERING_BACKEND=OpenGL
make
sudo make install
```

### GDCM 2.6.9

```bash
cd /tmp/dependencies
wget -O GDCM-2.6.9.tar.gz https://github.com/malaterre/GDCM/archive/v2.6.9.tar.gz
tar xf GDCM-2.6.9.tar.gz
mkdir build-GDCM
cd build-GDCM
cmake ../GDCM-2.6.9 -DGDCM_BUILD_SHARED_LIBS=TRUE -DGDCM_USE_VTK=TRUE -DCMAKE_INSTALL_PREFIX="/usr/local"
make
sudo make install
```

### ITK 4.9.1

```bash
cd /tmp/dependencies
wget http://iweb.dl.sourceforge.net/project/itk/itk/4.9/InsightToolkit-4.9.1.tar.xz
tar xf InsightToolkit-4.9.1.tar.xz
mkdir build-ITK
cd build-ITK
cmake ../InsightToolkit-4.9.1 -DBUILD_SHARED_LIBS=TRUE -DBUILD_EXAMPLES=FALSE -DBUILD_TESTING=FALSE  -DCMAKE_INSTALL_PREFIX="/usr/local"
make
sudo make install
```

## Build and install CamiTK 4 stable

You will need `imp` (the CamiTK workbench application), `actionstatemachine` (the protocol player) and all the libraries and tools.

### Build CamiTK

```bash
export CAMITK_LATEST="4.1.2"
workingDir=$(mktemp --tmpdir -d camitk-$CAMITK_LATEST-build-tmp.XXX)
cd $workingDir
wget https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/-/archive/$CAMITK_LATEST/CamiTK-$CAMITK_LATEST.tar.gz
tar xf CamiTK-$CAMITK_LATEST.tar.gz
mkdir build
cd build
cmake ../CamiTK-$CAMITK_LATEST -DCEP_IMAGING=TRUE -DCEP_MODELING=TRUE -DCEP_TUTORIALS=TRUE -DCMAKE_INSTALL_PREFIX="/usr/local"
make # To build in parallel: add -jX where X is your number of CPU
sudo make camitk-ce-global-install
```
Everything is now installed in `/usr/local` and should be accessible directly in your `PATH`.

## Install development tools
The CamiTK team recommands `kdevelop` IDE.

Install kdevelop and git:

```bash
sudo apt-get install kdevelop git
```


## Check the installation

Type:

```bash
camitk-config --config
```

You should get details information about camitk version, where camitk is looking for extensions, and the default installed extensions.
You're ready to run `camitk-wizard`!
