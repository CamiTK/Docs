# Setup CamiTK 4 Development Environment on Windows 7

Start from a fresh `Windows 7` install


## Development tools
The CamiTK team recommends `Visual Studio 2015` IDE.

Install Qt 5.6.1, Visual Studio 2015, CMake and git:

- [Visual Studio ](https://visualstudio.microsoft.com/fr/vs/older-downloads/) please take a look at this [issue](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/issues/78)
- [CMake 3.7](https://cmake.org/files)
- For git, follow the [git starter guide](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/Examples%20and%20Tutorials/Git_recipes/)
- [Qt 5.6.1](http://download.qt.io/archive/qt/5.6/5.6.1/qt-opensource-windows-x86-msvc2015_64-5.6.1.exe)


## CamiTK development environment

You will need `camitk-imp` (the CamiTK workbench application), `camitk-actionstatemachine` (the protocol player) and all the libraries and tools, all of this will be bundled in this package:

[CamiTK Developer Package](http://camitk.imag.fr/devdownloads/CamiTK-4.1.2_Burgundy_Developer.zip)

This installer will install the following libraries on your computer, in the `C:\dev` (64 bit)  directory:

- CamiTK 4.1.2 Community Edition source code
- The Community Edition required libraries:
    - VTK 6.3
    - ITK 4.9
    - GDCM 2.6.3
    - XSD 3.3.0
    - xerces-c 3.1.1

## Set environment variables

After the installation step, some environment variables **need** to be set. You have to modify the system variable `%PATH%` manually.

We recommend the installation of `Rapid Environment Editor` to help you do that.
Run `Rapid Environment Editor` with the administrator rights and edit the system wide `%PATH%` variable in order to add the path to all the dependencies:

- for example for a 64-bit system, add the following to the `%PATH%` variable:

```
    C:\dev\CamiTK\install;
    C:\dev\CamiTK\install\bin;
    C:\dev\Qt5.6\5.6\msvc2015_64\bin;
    C:\dev\Qt5.6\5.6\msvc2015_64\lib;
    C:\dev\xerces-c\3.1.1;
    C:\dev\xerces-c\3.1.1\bin;
    C:\dev\xerces-c\3.1.1\lib;
    C:\dev\XSD\3.3.0;
    C:\dev\XSD\3.3.0\bin;
    C:\dev\XSD\3.3.0\include;
    C:\dev\gdcm\2.6.3;
    C:\dev\gdcm\2.6.3\bin;
    C:\dev\gdcm\2.6.3\lib;
    C:\dev\gdcm\2.6.3\include;
    C:\dev\ITK\4.9;
    C:\dev\ITK\4.9\bin;
    C:\dev\VTK\6.3;
    C:\dev\VTK\6.3\bin;
```

!!! Note
    - The Qt path depends on where you installed Qt
    - The prefix  `C:\dev\..` also depends on where you installed the CamiTK developer package

!!! Warning
    We only support CamiTK installation on Windows for 64 bits using Visual Studio 2015. Visual Studio 2017 is known to work. If you decide to install another version of Visual Studio, do it at your own risk! You may ask for help on the forum but unfortunately we do not have a lot of free time to help developer with non officially supported configuration.

## Configuring, generating and compiling CamiTK 4

As you just install and set your environment for CamiTK, you can now launch the CMake generation and Visual compilation of CamiTK.

### configure and generate files to build

First run CMake GUI to be able to configure your project for compilation:

- In CMake GUI set the build and source directories
- Click `configure`
- Choose the correct compiler and check options you want: to get the full CamiTK Community Edition, we recommand to check the `CEP_IMAGING`, `CEP_MODELING` and `CEP_TUTORIALS` options. 
  Note: For future installation you can set the `CMAKE_INSTALL_PREFIX` to the directory where you want to install the _CamiTK 4_ binaries. We recommand for instance to set `CMAKE_INSTALL_PREFIX` to `C:\dev\CamiTK\install`.
- After that click again `configure` until red highlights are removed and you satisfied with the choices you made
- Click `generate` to obtain Visual Studio files in your build directory

### Compile

Go to the build directory and look for **camitkopensource.sln** file to open CamiTK Visual Studio solution to build. Then, compile the _ALL_BUILD_ project.

To install CamiTK you can just compile the _INSTALL_ target.


### Try to run CamiTK from your build

After the compilation you can use CamiTK, here is how to do with initial values for environment variables, adapt this with the choices you have made :

- **As a simple user**:
  binary executables are located in `C:\dev\CamiTK\install\bin` and launch camitk-imp.exe.
- **As a developer**:
  According to your Visual Studio configuration (debug or release), configure the camitkopensource solution property (right click on `ALL BUILD\Properties\Configuration Properties\Debugging\Command`) and add `C:\dev\CamiTK\install\bin\camitk-imp-debug.exe` or `C:\dev\CamiTK\install\bin\camitk-imp.exe`

## Check the installation

Type in a command shell:

```bash
camitk-config --config
```

You should get details information about CamiTK version, where CamiTK is looking for extensions, and the default installed extensions.
You're ready to run `camitk-wizard`!
