# Setup CamiTK 4 Development Environment

This is required to **develop a new CamiTK Extension Project**.
Installing the developer environment depends on the OS on which you are planning to develop.

We recommend that you use the last stable version of CamiTK (see below for the _bleeding edge_ version)

## Setup the stable development environment

Currently the CamiTK team supports the following OS:

- [Setup CamiTK Development Environment on Debian Stable (stretch)](./debian-stable)

- [Setup CamiTK Development Environment on Ubuntu LTS (16.04 or 18.04)](./ubuntu-lts)

- [Setup CamiTK Development Environment on Debian Old Stable (jessie)](./debian-old)

- [Setup CamiTK Development Environment on Windows 7](./win-7)

CamiTK also works on MacOS, but we could not spare any time to write the documentation on how to setup the environment for MacOS (do not hesitate to help!)

## Setup the CamiTK bleeding edge development environment

The bleeding edge version of CamiTK is the currently developed version (aka the `develop` branch). This is not a stable version and it comes with no guarantee... But all the newest and cool features!

We advise you to use the `develop` branch if and only if:
- either the current stable version is missing an *absolutely required* feature (something you really really need in your project)
- or you would like to participate in the development of the next version of CamiTK
- or you are curious and want to help us test all the new features before they are released to ensure minimal number of bugs

If you just want to get on working, just use the stable version.

For the `develop` version we recommend to use debian stable, Ubuntu 17.04 or later:

- [Setup bleeding edge CamiTK Development Environment on Debian Stable, Ubuntu 17.04 or later](./install-camitk-develop)
