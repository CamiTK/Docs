# Setup CamiTK 4 Development Environment on Debian Stable (Stretch)

Start from a fresh `debian-stable` install

## Development tools
The CamiTK team recommands `kdevelop` IDE.

Install kdevelop and git:

```bash
sudo apt-get install kdevelop git
```

## CamiTK development environment

You will need `imp` (the CamiTK workbench application), `actionstatemachine` (the protocol player) and all the libraries and tools.

### Install dependencies

```bash
export CAMITK_LATEST="4.1.2"
workingDir=$(mktemp --tmpdir -d camitk-$CAMITK_LATEST-build-tmp.XXX)
cd $workingDir
wget https://salsa.debian.org/med-team/camitk/raw/debian/$CAMITK_LATEST-1/debian/control
sudo apt update
sudo mk-build-deps -t "apt -y --no-install-recommends -o Debug::pkgProblemResolver=yes" --install ./control
```


!!! Note
    if you get the following error message
    ```bash
        E: You must put some 'source' URIs in your sources.list
    ```
    you need to add source repository, see the [debian documentation](https://wiki.debian.org/SourcesList)


### Build CamiTK

```bash
cd $workingDir
wget https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/-/archive/$CAMITK_LATEST/CamiTK-$CAMITK_LATEST.tar.gz
tar xf CamiTK-$CAMITK_LATEST.tar.gz
mkdir build
cd build
cmake ../CamiTK-$CAMITK_LATEST -DCEP_IMAGING=TRUE -DCEP_MODELING=TRUE -DCEP_TUTORIALS=TRUE -DCMAKE_INSTALL_PREFIX="/usr/local"
make # To build in parallel: add -jX where X is your number of CPU
sudo make camitk-ce-global-install
```

<!-- When debian stable was up to date, it was just a one liner:

```bash
sudo apt-get install camitk-imp camitk-actionstatemachine libcamitk-dev
```
Note: if you get the following error message

```bash
E: You must put some 'source' URIs in your sources.list
```
you need to add source repository, see the [debian documentation](https://wiki.debian.org/SourcesList)


-->



## Check the installation

Type:

```bash
camitk-config --config
```

You should get details information about camitk version, where camitk is looking for extensions, and the default installed extensions.
You're ready to run `camitk-wizard`!
