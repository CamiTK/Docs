Reporting bugs is a valuable contribution to CamiTK.

However, to write helpful bug reports and thus help the whole CamiTK
community, some guidelines should be carefully followed. Indeed, a bad
bug report containing not enough information may be at best totally
useless, or worst, would make CamiTK developers spend too much precious
coding time on triaging bug reports.

This is why you must follow this guideline to report a bug on
CamiTK.

!!! warning.png
    Any bug report not following these instructions may be closed with the reason **NOT ENOUGH INFORMATION** and without any further process.

## Information to Collect Before Submitting a Bug

### Check that your bug is not already discussed on the Forum

First of all, you may check the already being discussed [bugs](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/issues), to see if yours is not already addressed there.

!!! tip
    If you are new to reporting bugs here or elsewhere, you may want to try getting help from the more experienced contributors or first ask your question on the [How to ask for help](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/CamiTK%20Overviews/How_To_Ask_For_Help/). You might also find this two articles of interest:
    
    * http://mattgemmell.com/what-have-you-tried/
    * http://www.catb.org/~esr/faqs/smart-questions.html

### Check CamiTK version and installation settings

Use the `camitk-config` application to get information about your
installed version of CamiTK

#### On Linux

  - On a terminal and type:


```bash
camitk-config --config
```

From **CamiTK 3.5**, you can simply use the `--bug-report-info` option of
camitk-config to generate a bug report template, or click on “Report
Bug” in the “Help → About” camitk-imp dialog box.

From the command line:
```bash
camitk-config --bug-report-info
```

#### On Windows

  - Open a command shell (Start→search→cmd)
  - Go to the directory where CamiTK is installed, which is generally

```bash
cd C:\dev\CamiTK\install\bin
```

  - Run the camitk-config program with the `--config` option

```bash
camitk-config.exe --config
```

#### On Mac OSX

  - Open a terminal
  - run the following command (you may have to use the absolute path to
    camitk-config if you did not install CamiTK in /usr/local/bin or any
    other default paths).

```bash
camitk-config.exe --config
```

#### On all Systems

The results of [camitk-config](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/CamiTK%20Tools/CamiTK-config/) command is very instructive. With it you can:
  - Check that your directories are correctly set.
  - Check that all the application-action (i.e., the action of the
    application family) can be loaded
  - Copy the resulting text to paste it within your bug report if none
    of the above check pointed you to a regular installation problem

## Where to report?

There is a main bug tracker for CamiTK: [Gricad-Gitlab issues tracking system](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/issues).

But if you think that the issue is linked with the debian packaging, then the best is to use the [CamiTK Debian package tracking system](https://tracker.debian.org/pkg/camitk).

## Reporting a bug on [Gricad-Gitlab issues tracking system](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/issues)

### Check that your bug has not already been reported

Once your are logged in [Gricad-Gitlab isssues tracking system](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/issues), you can search bugs (see screenshot below) with keywords. Make sure that your bug is not already filed.

![Search issue](./assets/search_issue.png)

If the bug is already reported, check the description and read the related discussion. If there is some variation on what you observes on your configuration (OS, CamiTK version, anything else), please post your additional information. That might help us. Knowing that the bug was encountered by more than one person is also of high relevance.

### Find out which product is concerned

Make sure you made everything possible on your side to isolate the bug to something due to CamiTK itself and not your own extension/CEP or a third-party library. If you need help on how your own extension links to CamiTK, please consider [How to ask for help](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/CamiTK%20Overviews/How_To_Ask_For_Help/).

If you use CamiTK Developer version, try to use the debugger to find out
the name of the file responsible for the bug.

### While Reporting a Bug

#### Focus on One Issue

Only report one issue per bug report. Do not provide multiple issues within one report. CamiTK team will use the bug report id to follow up the bug correction process (related commit on Git, documentation, etc) which becomes extremely difficult if a single report concerns several problems. Bug solving is fasten if the bug report is well focused.

The next section gives you the list of things that are absolutely mandatory to provide in a bug report.

!!! note
    Note that since CamiTK 3.5, you can use camitk-config command to generate this pattern with all configuration informations included. For that, use the `--bug-report-info` option (or use -b) of the “camitk-config” command, in a console at the camitk/bin directory, and copy/paste the output in your bug description.

    You can also click on “Report Bug” in the “Help → About” dialog of Camitk-imp to generate a bug report template. See also the explanation about the bug report template below.

    From CamiTK 3.5, using the command line:

    `camitk-config --bug-report-info` Or click on “Report Bug” in the “Help → About” dialog of Camitk-imp to generate a bug report template.

#### Information to fill in

!!! attention
    Don't remove this tag: `/label ~Bug` from the generated report.

##### About you:

Please give us some light details about you.

###### Overview:
The goal of the summary is to allow CamiTK developers to rapidly sort
and affect bug to be fixed. It should be a short sentence summing up the
important points of the bug, for example,

>The CamiTK-Imp application crashes when X-action is applied on Y-component with parameters the a=1, b=4 and c=25.3

##### A summary simply telling:

>CamiTK doesn't work

is not enough, as we do not know:

  - what application does not work ?
  - what does not work (compilation, execution, action application, component opening, etc.) ?
  - when does not it work (on a certain component opening, on each File→Open call, etc. ) ?

Imagine a CamiTK developer who wants to know if (s)he is capable of fixing this bug… Does your summary help her/him ?

#### CamiTK Version

Print the result of `camitk-config` `--config` at least up until the
list of registered components and actions. Add the registered components
and actions if they help understanding your bug.

#### Steps to Reproduce

Most of the time, it is unclear what is causing a bug/problem. Thus, to
fix a bug, CamiTK team needs to reproduce the exact steps that lead to
it. You should not only describe the environment on which the bug
happened, but also what were the precise steps are performed to
reproduce it. Even better, if you compiled CamiTK in debug mode, you may
additionally write the debugger stack trace and the name and line of the
file where the bug occurs (the code line number makes sense only if you
specify the exact svn version of CamiTK you are using).

A one-time bug is not reproducible, it cannot be
fixed.

!!! warning
    If you cannot write a detailed step-by-step to reproduce a bug, and cannot characterize it, consider holding off on filing a bug report until you have a better understanding and can describe it more specifically.


#### Actual Results vs Expected Results

You should write in this section what you obtained when you follow the
step-by-step given at the previous paragraph. For this, you should use

  - textual description
  - copy/paste of the error message
  - screenshot of the result (as an attachment to your bug report)

To really understand your bug and help fixing it, you may also provide
what result you expected and in what aspect the result you obtained is
different from the one you expected.

In this section, you should be as objective and as factual as possible.

### Possible Interpretation

Your interpretation of where the bug may come from is very welcome and
would be of great help for CamiTK team to solve it. However be careful
to write it separately from the factual previous information to not
confuse the message.

#### Bug Report Description Overview Template

You may copy/paste and fill-in/replace the following text template in
your bug report
description:

!!! tip
    ```text
        About you: [Present yourself and your project you’re working on with CamiTK]
        Overview:  [Rewrite here a larger and more detailed restatement of your summary]
        CamiTK Version: [Print the result of camitk-config –config]
        Steps to Reproduce: [Write here the step-by-step process to reproduce the bug, including file to test (you can attach file on BugZilla / the forum)]
        Actual VS Expected Result: [Write here the result of the step-by-step process and explain why it is not what you expected]
        Interpretation: [Write here your interpretation of this bug]
    ```

#### Attachment

You may want to attach a screenshot of the error message obtained, as well as input data which lead to the bug.

### After Bug Submission

#### Be available to give feedback if needed

!!! note
    Do not submit a bug if you are not really willing for it to be fixed. |

Filing a bug will start the process of finding out how critical it is (which in itself takes time of our limited resources) and how to solve it. By submitting a bug, you thus commit yourself to follow up this submission and answer the questions asked by anyone about this bug. If you are not willing to contribute in that way, please do not submit reports as it will be generating useless workload.

!!! tip
    Thank you for contributing to CamiTK high level of quality !

# Sources and References

This description has been inspired by the following posts:

  - [https://developer.mozilla.org/en-US/docs/Mozilla/QA/Bug_writing_guidelines](https://developer.mozilla.org/en-US/docs/Mozilla/QA/Bug_writing_guidelines)
  - [http://stackoverflow.com/questions/240323/how-to-report-bugs-the-smart-way](http://stackoverflow.com/questions/240323/how-to-report-bugs-the-smart-way)
  - [https://help.ubuntu.com/community/ReportingBugs](https://help.ubuntu.com/community/ReportingBugs)
  - [https://wiki.ubuntu.com/Bugs/BestPractices](https://wiki.ubuntu.com/Bugs/BestPractices)
  - [http://blogs.fsfe.org/myriam/2011/10/20/when-is-a-bug-report-useful/](http://blogs.fsfe.org/myriam/2011/10/20/when-is-a-bug-report-useful/)
