# CamiTK Extension Project Developer Guide

This guide will help you start developping new extension in CamiTK

## Documentation for developer

Here are some useful documentation:

- Use [the CamiTK log system](../Highlighted Features/Log-System) to simplify logging and debugging
- Use [the CamiTK automatic tests framework](../Highlighted Features/Automatic-Tests) to automatically generate tests for your extensions

## Migration guides
- [CamiTK 3.x → 4.0 migration guide](../CamiTK Versions/Migration Guides/Migration-Guide-3.x-to-4.0)
- [CamiTK 4.0 → 4.1 migration guide](../CamiTK Versions/Migration Guides/Migration-Guide-4.0-to-4.1)

## Source code management

- [Git recipes](./Examples and Tutorials/Git_recipes)
- [CamiTK branching model](./CamiTK Overviews/CamiTK_Git_Branching_Model)
- [Git Bash helper scripts](./Examples and Tutorials/Git_Bash_Helper_Scripts)

## CamiTK Programming Guidelines

The version 1.0 (June 2018) of the guidelines are [available in PDF](assets/CamiTK%20Programming%20Guidelines%20v1.pdf). If you intend to develop long term and reusable CEP or a CEP that you would like us to include in the CamiTK Community Edition, please read and conform to these guidelines.

---

## Continuous Integration status

- `develop` branch current status

[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/badges/develop/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/develop/commits/test1)
[![coverage report](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/badges/develop/coverage.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/commits/develop)
