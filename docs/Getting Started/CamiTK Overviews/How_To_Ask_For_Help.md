If you are facing a difficulty while developing in CamiTK, you may want
to ask more experimented users for help. Here is explained how to do so.

## Make sure your question is not already answered

### [On the Forge](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/issues)

You may use the search engine on top of the page. And to be able to use all the features it is recommended to create an account on this [page](https://gricad-gitlab.univ-grenoble-alpes.fr/).

### [On the FAQ](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/FAQ/FAQ/)

### [On the API Documentation](http://camitk.imag.fr/apiDocumentation.php)

The API documentation features lots of information. You may use the
**Modules** tab to browse the different CamITK source code content or
directlty search on the engine in the top right corner.

### [On the Tips & Tricks section]("../../Examples%20and%20Tutorials/Tips_And_Tricks.md")

This section feature developers level helps on several subject, do not
hesitate to check it out\!

## Check CamiTK version and installation settings before posting

Use the `camitk-config` application to get information about your
installed version of CamiTK

### On Linux

  - On a command line tool (shell, bash, etc.) type:

```bash
camitk-config --config
```

### On Windows

  - Open a Command tool (Start-\>search-\>cmd)
  - Go to the directory where CamiTK is installed, which is motsly

```bash
cd C:\dev\CamiTK\install\bin
```

  - Run the Camitk-config program with the `--config` option

```bash
camitk-config.exe --config
```

### On Mac OSX

  - Open a Terminal
  - run the following command (you may have to preceded it with the full
    absolute path if you did not install CamiTK in /usr/local/bin).

```bash
camitk-config --config
```

### On all Systems

  - Check that your directories are correctly set.
  - Check that all the application-action can be loaded
  - Copy the resulting text to paste it within your post.

## About the Form

In order for us to betterly understand your issues and for the solution
to help everyone reading your post, please post:

  - In English
  - Present yourself, the institution you’re working for (university,
    laboratory, company, free lance enterprise, etc.) .
  - Be Polite
  - Try to be clear / meaningful. Try to explain the step you’ve done
    until you encounter your problem. Simply telling **one thing doesn’t
    work** doesn’t help.

References:

  - <http://www.kde.org/code-of-conduct/>
  - <http://www.catb.org/esr/faqs/smart-questions.html>
  - userbase.kde.org/Asking\_Questions
