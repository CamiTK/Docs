# CamiTK architecture

Computer Assisted Medical Intervention (CAMI hereafter) is a complex
multi-disciplinary field. CAMI research requires the collaboration of
experts in several fields as diverse as medicine, computer science,
mathematics, instrumentation, signal processing, mechanics, modeling,
automatics, optics, etc. CamiTK is a modular framework that helps
researchers and clinicians to collaborate together in order to prototype
CAMI applications by regrouping the knowledge and expertise from each
discipline. It is an open-source, cross-platform generic and modular
tool written in C++ which can handle medical images, surgical
navigation, biomedicals simulations and robot control.

This page presents what is the software architecture of CamiTK (Computer
Assisted Medical Intervention Tool Kit), and focus more specifically on
the CamiTK SDK, its content and how it can be used to develop new
extensions.

The goal of the CamiTK project is to gather not only the knowledge of
the specialists from each area of the CAMI field, but also to gather
their know-how by facilitating data exchange, software prototyping of
applications and thus leading faster and more efficiently toward
clinical validation and benefit to the patient. CamiTK’s general design
is inspired by the Component-Based Software Engineering (CBSE) concept
and therefore encourages reusing rather than reinventing the wheel.
Rapid prototyping of CAMI applications is made easy by assembling
pre-built CamiTK components (called CamiTK extensions) instead of
continuously patching onto existing code.

When we say “do not reinvent the wheel”, we really mean it\! It is so
true, that in fact, CamiTK provides the wheel (and some useful
extensions to this wheel):

<center>
![CamiTKWheel](./assets/CamiTKWheel.png)
</center>

Don’t worry if this figure is not clear, this page tries to explain all
of the things that are on this figure:

1.  The central wheel (CamiTK Core and Service Layer),
2.  The puzzle pieces attached to the wheel (in CamiTK they are called
    “*extensions*”, their objectives, and the signification of their
    different colours,
3.  and last, but not least, the glue and expert code (all yours\!).

For a quick introduction to all this, you can also refer to the [video
presentation](http://camitk.imag.fr/videoPresentation.php).

CamiTK can be seen three different ways (hence the logo):

![CamiTKThreeSides](./assets/CamiTKThreeSides.png)

Open Project : CamiTK is an *Open Source project*. You probably now that
well if you read this page. Not only the essential source code is
available under the LGPL-v3 license, but there are also a lot of
collaborative tool, the software forge being the first of them, where
anyone can help (you don’t need to be a programmer for that, there are
lots of area where improvements are needed, not only in the source
code\!)
CamiTK For User : CamiTK directly provides CAMI applications, that can
be useful to *do something* immediately. The installation is easy (there
are packages for linux, and windows for example).
CamiTK For Developers : CamiTK is a framework, and developers can create
their own pieces of puzzle thanks to the Open Source *Sofware
Development Kit (SDK)*

## CamiTK Architecture

CamiTK is extensively using Component-Based Software Engineering (CBSE).
It organizes the development of new features in three different levels
(hence the logo):

![CamiTKThreeSides](./assets/CamiTKLogoLayers.png)

CamiTK Core : this is where all the concepts are defined and
implemented. Only the CamiTK experts are supposed to understand it
well.
CamiTK Service Layer : this is the definition of how to glue the source
code developed in/for a specific field and CamiTK. All developers need a
basic knowledge of the service layer. Note that camitk-wizard
facilitates the work of the developer and that CamiTK SDK itself is
developed with simplicity in mind. You will acquire this knowledge
little by little. Most of the usage are covered by default behaviours
already implemented in CamiTK SDK helper classes (this will be
transparent for you).
CamiTK Extensions : this is the domain specific code. This is where most
of the work has to be done in order to provide a new feature in CamiTK
for a specific field. This is where you should focus your attention as a
developer. You will see that you will mostly be using your own
expertise.

CamiTK SDK (for Software Development Kit) provides CamiTK Core and the
service layer. Developers (you) provide all the extensions needed in a
specific research field or Computer Assisted Medical Intervention
project.

CamiTK SDK provides:

  - the CamiTK core library, main applications and minimal set of
    actions and components.
  - a Service Layer (an extensible mechanism with 4 possible types of
    extensions).
  - an application builder: you just have to assemble some of provided
    application elements (e.g. viewers, interactors…) to build your own
    application.

## Create a new extension

![ServiceLayerGlue](./assets/ServiceLayerGlue.png)

Building an extension is easy and is simplified by using the wizard to
generate the code skeleton and build it on your development platform.
Once an extension is build and ready, it can be automatically integrated
with all the existing extension, i.e., once ready you can start using
your extension immediately.

CamiTK does not want you to modify any of your own code. Your code is
your speciality, and you are the only one to know what is best to do
there. Instead CamiTK asks you to provide a **glue** to the service
layer: you need to *encapsulate* your work in a pre-define extension
type. This generally means a minimal or no modification of your own
code. There is no modification needed if you already have a well defined
API where the GUI is completely separated from the rest. This is
generally the case if you have followed well known/recommended software
design. If not, your code can only gain from the new design\!

An extension in CamiTK is distributed as a plugin. There are four types
of possible plugins in the CamiTK architecture.

The learning curve for CamiTK should therefore be minimal.

### Extension Projects

To simplify the development and distribution of extensions, CamiTK
extensions are grouped in [CEP (CamiTK Extension Project)](../CamiTK_Extension_Project). A CEP basically regroups all the extensions
developed by a specific person or group for a specific field or CAMI
project. CEP facilitates task oriented project and helps you to focus on
the most important/valuable code.

It also provides a natural way of maturing your code.

![CEPIncubator](./assets/CEPIncubator.png)

### Extension types

There are four types of extensions:

  - Component extensions, they provide data I/O features,
  - Action extensions, they provide data processing features,
  - MainWindow extensions, they provide interaction and visualization
    features, and
  - Viewer extensions, they provide a way to design a GUI main window.

![ServiceLayerCamiTK](./assets/ServiceLayerCamiTK.png)

These are the four types of glue available in CamiTK. In a project you
need to specify a given number of extensions of each types. Most of the
cases are covered by action and component extensions. The Service Layer
provides the definition of all these types of extensions, *and* the SDK
provides a lot of default behaviours for the extensions. The default
behaviours are usually the one you need in 90% of the time. for the
remaining 10%, you will have to override the default behaviour (in
Object Oriented Programming, this will lead to redefinition of an
inherited method).

These four extensions correspond to four well-known principles in
software architecture design, and lead to the separation between,
respectively:

  - Processing logic
  - Data logic
  - Application logic
  - Presentation and interaction logic

![ServiceLayerCS.png](./assets/ServiceLayerCS.png)

Please read [the tutorial help page](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/Examples%20and%20Tutorials/CamiTK_development_tutorial/) to start developing your
own extensions. The CamiTK open source project has a dedicated
*tutorial* CEP that contains loads of source code examples.

### Which extension type should I consider?

The table below gives you a list of use cases for the four types of
extensions.

| Type | Description/Need | Examples | Solutions |
|---|---|---|---|
| ![ServiceLayerCamiTK.png](./assets/Component.png) | - View a new mesh, graph scene or image format</br>- Connect a new device to view its data, control its parameters/actions | - Import .obj format</br>- View 3D Ultrasound images</br>- Connect a medical robot or sensor</br>- Connect a motion tracker device to interact with the 3D viewer |- Create a new component extension that inherits from basic ones (MeshComponent, ImageComponent)</br>Or Create a new generic component extension</br>Or combine existing components in a new one |
| ![Action.png](./assets/Action.png) | - Transform some data</br>- Generate new information from some data</br>A new way of interacting with available view | - Image filter, volume reconstruction</br>- Biomechanical simulation using a specific method</br>- Export a data to a specific format</br>- Mesh/image registration</br>- Fly camera in 3D using a path of key position and timer</br>- Take regular screenshots of one viewer to create a movie</br> | 1 - Choose which component is concerned (what is the input data of the new algorithm)</br>2 - Create a new action extension |
| ![Application.png](./assets/Application.png)|- You need a specific GUI for your specific CAMI application | - An application that guides the user through a pipeline of predefined actions </br> - No 2D/3D interaction, just data explorer and property viewer</br> - A Specific application to test on a group of user (including specific log</br> - or trace)</br> | - Use the provided Action State Machine application</br>Or</br>choose which software component you need in the main window and</br>create a new MainWindow extension|
| ![Viewer.png](./assets/Viewer.png)|- New type of interaction with the data</br>- A new way of visualizing the data |- Graph of values computed by an action</br>Histogram of data from a component</br> |- Create a new viewer extension</br>Or inherit from an existing one and add/redefine a behavior</br> |

: CamiTK extension use cases: what type of extension should you
consider, depending on your need

## How can I apply an action to a component?

CamiTK provides all the mechanisms to automatically present to the user
the available actions considering the currently selected component.

  - If you are using an interactive application (e.g., camitk-imp),
    first open and select some component instance, and then click on the
    action menu, use the right-click to popup the contextual menu, or
    use the action viewer to browse the list of available actions.
  - If you are not using an interactive application (e.g., you are
    running a kind of script), then you need to do everything
    programmatically using the CamiTK API (there are simple method to
    select a component instance and trigger a given action).
  - If you want to run an action pipeline (e.g., your are using the
    action state machine) , simply define the in/out data of the
    pipelined actions

Note : the actions that are only creating data (for example: open a
file, generate a generic cubic mesh…) can be applied when no components
are selected.

## Extension repositories

But where are all these marvelous extensions installed?

At the moment there are three possibles extension registries, also
called installation directories, (hence the logo):

  - The global repository: this is where CamiTK SDK is installed
  - The local repository: it is only available for the current user, it
    is installed in the user configuration directory (application data
    on Windows, $HOME/.config on Unix and MacOs)
  - The build repository: this is the build tree currently used by the
    developer to create its new extensions

When developing a CEP, it will be found in the build repository. Once
you are satisfied with it you can either install it in the global or
local repository.

Important : when you develop a CEP, do not forget to specify the
“current working directory” to your build directory. When camitk-imp
is run, this will help CamiTK to find all your extensions currently in
development without the need to run the local/global install target all
the time.

## CamiTK Open Source ecosystem

The CamiTK Open Source [CEP Set](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Getting%20Started/CamiTK%20Overviews/CamiTK_Extension_Project/) provides:

  - the **SDK (Sofware Development Kit)** CEP is the base of all CamiTK
    development. It provides the CamiTK core library, some basic but
    useful component and action extensions as well four applications
    (see details below). It is required to do any development in CamiTK.
    The SDK is based on VTK Qt (and CodeSynthesis XSD), *and that’s
    all*. Additionally to CamiTK core, it provides two useful libraries:
    coreschema (the CamiTK Core XML data-binding library),
    qtpropertybrowser (to build GUI for Qt’s meta-property).
  - the **Imaging** CEP contains all the action and component extensions
    that are useful for processing medical imaging. It provides the
    dicom and itkimage components, the itk filters and itk segmentation
    actions. It depends on… ITK.
  - the **Modeling** CEP contains all the action and component
    extensions that are useful for biomechanical processing. It provides
    the LML,PML and MML libraries, and the related components and
    actions. It also provides qcustomplot (a simple but powerful plot
    widget for Qt).

The CamiTK SDK is called that because not only you get the fundamental
source code and the CamiTK Core library, where the service layer is
defined, but you also get four CamiTK executables that are needed to
help you develop your own Computer Assisted Medical Intervention
application: camitk-imp, camitk-config, camitk-wizard and
camitk-actionstatemachine.
