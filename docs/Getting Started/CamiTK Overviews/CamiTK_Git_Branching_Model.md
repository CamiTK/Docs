Here, we see what is the adopted branching model for the CamiTK project.
It is based on the great steps of software development and freely
inspired from nvie[^1] which is describe a usefull workflow. A deep
description can be found on this site[^2] too.
To improve your knowledge on Git you can consult some of these
references like the online Pro Git book[^3], a visual reference[^4] or
this guide[^5].

## Central Repository to Coordinate Development

We maintain a central repository, which is not central as SVN can be,
but as it allows synchronisation for everybody on objectives to reach
(releases, hotfixes, common work, etc.). It is the common history of
CamiTK development.

We structured branches on our Git system as :

  - Production release
  - Feature conception
  - Debugging
  - Release improvement via hot fixes
  - Common developments

We mainly follow what is called the *gitflow* workflow, check [this
quick and illustrated
explanation](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

To get a more convenient way for using our scm, we apply a **merge
request** system based on a **forked** repository, and all the work
below described is deported on a forked repository. Thus, our central
repository contains only two branches :

  - master
  - develop which is the default branch

To apply the below described branch system we have to fork initial
repository. To do this see this page :
[Contribute to CamITK](../../Getting%20Started%20Guides/Contribute_to_CamITK/). After you
have forked the initial repository you will get your own central depot.

## Working with depot

At this central depot, some of these branches are permanent (Master and
Develop) and others are temporary (Releases, Hotfixes and Features)
branches. In other side, some branches may only exist on the local
working tree of a developer or group of developers and can’t be viewed
on the central repository. That is mean some sub-team can work in
parallel out of the main development flow. After their work is in a
final state, they may want to merge back or create a new repository that
they own. It’s up to them.

The repository is just a place where we manage our collective
development. The picture below represent how the repository is
structured. The two little repositories under the big one are local
repositories, as you can have on your computer. The way you choose to
develop with Git is depending on what you want to do. In the rest of
this document we consider the CamiTK team point of view.

The big one is the repository you will clone when you want to
participate in the development which is the one you forked. For you,
this is the remote repository. We describe it as the “origin”. This name
prefix the branch name, for instance, origin/develop is the develop
branch on the remote repository. Actually, origin is used to make an
alias for the complete path to the remote branch at the remote
repository. Thus, in the rest of this document, if you read only the
branch name without origin it will stand for the local branch, otherwise
it will be the remote branch of what we talk to.

<center>

![CamiTK : central repository branching model](./assets/RefentielGit.png "central repository branching model")

</center>

As we just see the global structure, let’s go on specific description of
each branch.

## Develop and master branches

### Develop branch

Origin/develop is where everyone can find the source code in last
development state. All the standard development activities come from
this branch and merge back to this branch. Thus, if you want to make new
feature you checkout the origin/develop to a feature branch on your
local work-tree. If some improvements will be made on the origin/master
branch they have to be merged back to origin/develop in order to keep
this branch up to date with all new development.

### Master branch

Origin/master is where released versions or Hotfixes are stored. A tag
is defined for every release or hotfix. This is the integration branch,
no one can push on this branch except authorised developers. If want to
get the latest stable version, you can clone from this branch.

## Feature branch

The feature branches are for making effective development works. It is
the developer job to create it on his local work-tree from the develop
branch. Thus, the development work is done without improving integration
risk on develop branch and with more flexibility for the developers
itself on its local work-tree. When the work is done feature branch is
merged back to develop branch and pushed on the origin/develop. The case
in which developers collaborate on a huge feature is planned. Our
branching model allows developers to push a feature branch to share it
with others over the referential.

## Release branch

The release branch is visible from all of the referential repository.
Some works from all team have to be done for the release can be
distributed. While a release branch is open features are freezing and
only debug can be performed and committed on this branch and back to the
develop branch. When the final state of the release is reached all these
works are committed back to the develop branch and on the master branch.
A tag is set onto the master and the develop branch. After that the
release branch is deleted.

## Hotfix branch

Hot fixes come from the necessity to produce a correction to make
release version already published stable. These developments will
involve a hotfix branch creation that comes from the master branch,
based on the release tag on which the hotfix must be applied.
Corrections works are made and hotfix branch is merged back to the
master branch with a hotfix tag and the develop branch too.

## Naming conventions

Here is the naming conventions adopted In CamiTK team to name branches
At the creation.

  - **Feature**: The feature branch should be named `feature/XXX` where `XXX` is a comprehensible
    (although rather short) description (e.g., `feature/connect-to-camitk-store`). Do not hesitate
    to use multiple words separated by `-` (dashes).

<!-- -->

  - **Bug fix**: The bug branch should be named `bug/XXX` where `XXX` is a comprehensible
        (although rather short) description (e.g., `bug/parallel-build`). Do not hesitate
    to use multiple words separated by `-` (dashes).

<!-- -->

  - **Hotfix**: the branch should be called `hotfix/x.y.z` (`hotfix/4.0.1` for example).

<!-- -->

  - **Release**: the branch should be called `release/x.y.z` (`release/4.0.0` for example)

<!-- -->

  - **Tag**: Use tag to add a label to the commit that correspond to the
    release or hotfix (e.g., for a release: `4.0.0` or `4.1.0`, and for
    a hotfix: `4.0.1` or `4.0.2`)

## References

[^1]: [“A successful Git branching
    model”](http://nvie.com/posts/a-successful-git-branching-model/)

[^2]: [“Comparing
    Workflows”](https://www.atlassian.com/git/tutorials/comparing-workflows)

[^3]: [“Pro Git”](https://git-scm.com/book/en/v2)

[^4]: [“Visual Git
    Reference”](https://marklodato.github.io/visual-git-guide/index-en.html)

[^5]: [“git- the simple
    guide”](http://rogerdudler.github.io/git-guide/index.html)
