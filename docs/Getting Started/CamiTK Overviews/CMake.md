![External Librarie](./assets/Extlib.png)


[CMake](http://en.wikipedia.org/wiki/CMake) is a cross-platform,
open-source system for managing the build process of software using a
compiler-independent method.
As CamiTK is multi-platform, when you download CamiTK sources it
features a **CMakeLists.txt** file, which CMake will use to build a
development environment according to your operating system. For
instance, it is used to build Makefiles for Linux and Visual Studio
solutions (.sln) for Windows.



# How it works

CamiTK is based on Kitware’s CMake buid system generator. CMake allows
the CamiTK project to use the same code on all the supported plateform
(Linux, Windows and Mac/OS). It provides a GUI for its configuration on
each platform. It also forces the separation between the source code and
the build objects (.o, .lib…) (which is a good thing\!)

![Cmake Based](./assets/Cmake-based.png)

When using cmake you need to understand that:

  - the build directory is separate
  - starting from scratch it needs to be populated first by cmake, this
    is called the configuration phase
  - then the developer uses its own compiler toolchain to build the
    libraries and executable (we recommand Visual Studio of Qt Studio on
    Windows, and kdevelop or the command line on Unix)

CamiTK provides a lot of very easy-to-use macros to help you writing
your extension. Most of the time you won’t need to edit the CMake
configuration files (these are all the CMakeLists.txt files).

# Installation

## Windows

CMake installation is very simple:

1.  [Download](http://www.cmake.org/cmake/resources/software.html) the     latest Windows binary distribution of CMake.
2.  Follow the installation instructions.


## Mac

Install [CMake For
MacOSX](http://www.cmake.org/cmake/resources/software.html) (check the
install command line tools links)


## Linux

Run in a terminal:

```bash
sudo apt-get install cmake cmake-qt-gui
```
