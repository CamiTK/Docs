<img class="fit-picture"
     src="../assets/CEP-logo.png"
     alt="CEP" height="150px">

**E**xtension **P**roject (CEP) is a new concept introduced in CamiTK
3.1. It is a way of managing source code tree as well as extension
archiving and distributing. In simple words: it is a way of organizing
your extensions.

A CEP helps maintaining the source code of any extension. A CEP can
contain the following things:

  - action extensions
  - component extensions
  - application extensions
  - viewer extensions
  - specific libraries that are needed for this CEP.

![CEP is yum](./assets/AvenueDuCep.jpg "Avenue du CEP") The main idea
is to group the actions, components, applications and viewers that are
specific to one scientific field. A CEP helps to define a perimeter for
the collaboration of a group of people who are working on a common
(research) project. It therefore simplifies dependency management: a CEP
should contain everything needed for building the provided actions or
component extensions (and if not, it should be built on top of another
CEP).

When a directory contains more that one CEP subdirectory, we call that a
**CEP Set** (CEPS).

Before you go any further, it is greatly advised to first read more
about the [CamiTK architecture](./CamiTK_architecture.md "CamiTK architecture").

## Open-Source CEPS

For instance the open-source CamiTK project is distributed as one CEPS
that contains three CEP:

  - the **SDK (Sofware Development Kit)** CEP is the base of all CamiTK
    development. It provides the CamiTK core library, some basic but
    useful component and action extensions as well four applications
    (see details below). It is required to do any development in CamiTK.
    The SDK is based on VTK, Qt (and CodeSynthesis XSD), *and that’s
    all*. Additionally to CamiTK core, it provides two useful libraries:
    coreschema (the CamiTK Core XML data-binding library),
    qtpropertybrowser (to build GUI for Qt’s meta-property).
  - the **Imaging** CEP contains all the action and component extensions
    that are useful for processing medical imaging. It provides the
    dicom and itkimage components, the itk filters and itk segmentation
    actions. It depends on ITK.
  - the **Modeling** CEP contains all the action and component
    extensions that are useful for biomechanical processing. It provides
    the LML,PML and MML libraries, and the related components and
    actions. It also provides qcustomplot (a simple but powerful plot
    widget for Qt).

Additionally, two other CEP are available online:

  - the **nonfree** CEP contains additional CamiTK extensions intended
    to work with the CamiTK open-source that do not comply with the DFSG
    (\[<http://www.debian.org/social_contract#guidelines>| Debian Free
    Software Guidelines\]) or have other tricky code that make their
    distribution problematic. They may not comply with all of the policy
    requirements for the rest of the CamiTK Open-Source CEPs due to
    restrictions on modifications or other limitations. For instance, it
    contains the source code of the tetgen library, a tetrahedral mesh
    generator, and the related action extension that allow you to use it
    on any mesh in CamiTK.
  - the **tutorials** CEP contains example code. This is where you
    should look for C++ examples on how to do things.

## CEP directory tree
<center>

![CEP source structure](./assets/CEP-tree.png "CEP Source structure")

</center>
This picture shows an example of the recommended tree structure for a
CEP. You do not *have to* do this, but it is what we use for our CEP
(even for the Open-Source CEPS), and what we recommend. See also at the
end of this section, the little note about CEPS.

This example shows a CEP called *cepname*. Everything useful for CEP
development is inside a directory called *cepname*.

There are three sub-directories at the top level of *cepname*:

  - subdirectory **src** contains *all* the CEP **source code**. You can
    also name this directory **cepname**, which can facilitate the use
    of a VCS.
  - subdirectory **doc** contains all the documentation about this cep
  - subdirectory **build** is the build directory needed by CMake, it
    contains all generated files and can be deleted/regenerated at any
    time (notice that on this screenshot it is in italic, which means it
    is a link to a temporary directory: this is a waste of time and
    space to backup the build directory as it can be regenerated from
    scratch).

#### Source subdirectory

The source subdirectory nonfree itself contains:

  - README is optional, it contains some general information about your
    CEP (this is not the same as the description found in
    CEPDescription.cmake). You can explain some specific things about
    your CEP in the README (this is good practice). It will be the first
    thing other CamiTK developers will read before bombarding you with
    questions (got the tip?)
  - COPYRIGHT contains the copyright information about the source code
    of the entire CEP. By default it follows the same COPYRIGHT as
    CamiTK (LGPL-v3), so if you have a different copyright **do not
    forget** to edit this file for reflecting yours.
  - the top-level CMakeLists.txt and the FindCamiTK.cmake files are
    needed to build the CEP (default version of this file should work
    out-of-the-box, once you have set up CEPDescription.cmake)
  - subdirectories actions components applications viewers contains all
    the extensions provided by the CEP. Depending on your CEP you can
    have any of those directories.
  - subdirectory libraries includes all the specific libraries that are
    needed for this CEP (on that see also [including a new library](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Advanced%20Topics/External_libraries_policy/)

#### Note on Copyrights and License

If your CEP is still in the incubator, you don’t need to think too hard
about its Copyright and the license under which it will
distributed.

!!! note
    When possible, we encourage you to do nothing, i.e., to leave the COPYRIGHT file untouched. Which means that your CEP will be available under the LGPL-v3 license. You don’t need to decide at the beginning if your CEP can/should be distributed as an Open-Source CEP, but it won’t hurt to start thinking about it. |

If for any reason you think you can not keep the LGPL-v3, please **do
not forget** to edit COPYRIGHT
accordingly.

#### Note on CEPS tree structures

!!! note
    For CEP Set, the build and install should be at the top level of the CEP Set, i.e. one level higher than the CEP directories. Consequently there is no src subdirectories. |

## CEP maturing

When you are first working on a CEP, this one might not be ready to be
publicly visible, this is why it might to be developed a bit more. This
step of maturation is called CEP “incubation”.

![center](./assets/CEPIncubator.png)

### Incubators location

Incubators are svn and Git location where you may host your source code to
mature it. At the moment, 2 incubators are proposed:
\* One public, where you may host your CEP containing extensions that
are open-source.

```bash
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:CamiTK/CamiTK-Incubator/albert.git
```


\* One private, where your code may not be visible from people out of
the TIMC laboratory. Use this incubator if your CEP keeps copyrighted
source
code

```bash
svn checkout svn+ssh://login@svn-timc.imag.fr/svn/gmcao/camitk/trunk/camitktimc/incubator-local
```


Do not forget to replace **login** with your actual login and to have
logged into your session your private RSA key (required on the svn-timc
server and optional but convenient on the forge server).

!!! tip
    If you create a CEP from scratch, use the CEP **skel** located at the root of any of the incubator. You will find information on how to adapt it to your own CEP in the **README** file.

### Maturation stage reached

Once you feel your CEP is ready for exhibition, you might want to store
it as:

  - an Open-Source CamiTK CEP (including nonfree if there is some
    specific license)
  - a part of an existing Open-Source CamiTK CEP (e.g., one of your
    image processing action can be moved to the imaging CEP)
  - a restricted CamiTK CEP because your code is linked with
    private/commercial license, or
  - a part of an existing restricted CamiTK CEP.

## CEP installation

But where are all these marvelous extensions installed?

At the moment there are three possibles extension registries, also
called installation directories, (hence the logo):

  - The global repository: this is where CamiTK SDK is installed
  - The local repository: it is only available for the current user, it
    is installed in the user configuration directory
    (%APPLICATION\_DATA% on Windows, $HOME/.config on Unix and MacOs)
  - The build repository: this is the build tree currently used by the
    developer to create its new extensions

When you are working on your CEP, the build objects (shared object…) are
in the build repository. This is good as you don’t want to install it
after every compilation. Once you have a stable version (and after you
use your VCS to publish it\!), you can install your CEP.

There are two specific type of installation:

cepname\_global\_install : this will install your CEP in the global
repository. You might need superuser privilege to do so (depending on
where the global repository is on your machine). If the SDK is
accessible to all users of the machine, the main advantage is the global
installation is that all users can then use your extension.
cepname\_local\_install : this will install your CEP in the local
repository (%APPLICATION\_DATA% on Windows, $HOME/.config on Unix and
MacOs). It will only be available to you.

If you do not want to install, you can still ask CamiTK to load your
extension automatically at runtime by *registering* your action or
component extension shared object (dll). To do so, run camitk-imp and go
to the preferences dialog, then manually add your action or component
extension. But as said above, during the development process,
installation is not needed, your extension will automatically be built
in the build repository and loaded automatically at runtime.

To summarize the concepts of CEP maturing and CEP installation, there
are several steps to follow in the aim of reaching a maturing stage. We
try here to describe in short the different steps to follow:

  - 1- Develop your code, in your ‘src’ directory
  - 2- Do your basic tests with the binaries compiled in the ‘build’
    directory
  - 3- If you want to test your code with a version already installed on
    your computer, do a ‘global-install’ or a ‘local-install’. Test
    again your changes.
  - 4- If you want to backup your modifications and do a tracking of it,
    commit your source code in the SVN incubator server.
  - 5- Once you think your work is mature enough to be released, you may
    decide to share it within CamiTK (OpenSource or non-free CEPs) or
    keep it within your organization (for Copyrights
purposes).



![Cep Installation Maturing](./assets/CepInstallationMaturing.png)

## CEP source package

Once you have configure your CEP using CMake you can use the specific
target **package\_source** to generate a source package containing all
your code. For instance on the command line (Linux, Mac) just type:

``` bash
  make package_source
```

In an IDE (kdevelop on Linux or MS Visual C++ on Windows for instance,
build the target **package\_source**).

It will produce a *.tar.gz* and *.zip* which contains the same as your
source directory. It will be named depending on the description you gave
in the CEPDescription.cmake file and the CamiTK version you are
currently using. For instance, the *nonfree* CEP produces two files:
camitknonfree-3.1.0-Source.zip and camitknonfree-3.1.0-Source.tar.gz.

## CEP binary package

Once your CEP is configured you can also generate a binary archive. The
binary archive is a very easy way, independent from the chosen source
code license, to distribute / send your extensions to any colleague,
fellow researcher, clinician, or any other type of user who will need /
want to check or use your code. Once you sent the CEP binary archive,
your correspondent just has to register your extensions in camitk-imp to
be able to start using it (of course if the platform matches).

To build a binary package for your CEP, on the command line, just go to
the build directory and type:

``` bash
  make camitk_extension_package
```

If you use an IDE, just run the **camitk\_extension\_package** target.

It will produce a *.cep* file, which in fact is a tar.gz archives with
the same directory structure as any CamiTK install directory.

## CEP from scratch

camitk-wizard provides a way to automatically build a source code
skeleton for a new CEP. You can also create a new CEP from
scratch.
