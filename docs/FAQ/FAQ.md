
## Coding

### How to handle several components within only one component extension

You have created a component extension ‘MyComponentExtension’ with
serveral components : ‘MyComponent1’, ‘MyComponent2’. The CamiTK wizard
asks you to select *only* one top level component.

It is **strongly** advised to have only one top level by component
extension. The top-level component is instanciated in when opening a
file corresponding to the managed extension.

### How to manage data without a file

In CamiTK, Component are used to manage all the data. The manage a
specific type of data you have to consider whether it is a generic type
of data, a type of mesh or a type of image (and coming soon: a type of
video camera, a type of pointer etc…). If your data type can be
assimilate to static data, i.e., it can be instanciate/initialize from
data coming from a file, the only thing you have to do is to create a
new Component extension and in the extension class, just declare which
file extension is used to load your data from a file.

However, sometimes you data are coming on the fly, or are acquired
directly from a device or are “programmatically” generated (e.g., using
a vtk\*Source class), without having any initial static file to open.

In this case, there are two ways to load data in CamiTK without having
to use any data file to start with:

1.  ) create an initializer action, or
2.  ) define a specific file type / model of the connection to your
    device (name, parameters of the connection, etc…)

#### Create an initializer action

This action is not specific to any component, i.e., that will be
available at any time, even when no component are in memory. In this
case you can trigger the action at any time in the *Action* menu of
camitk-imp. This action should :

  - initialize the connection to a device, and
  - create an empty specific **camitk::Component** or initialize this
    component from the connection

The action

  - can have parameters that will be taken into account when the “apply”
    button is clicked (see SphereTopology in
    sdk/actions/mesh/basictopology for an example)
  - can have no parameters, in this case the apply() method is run
    immediately when the action is triggered (see CloseAction in
    sdk/actions/application)

#### Define a specific file type

This can help you generalize an acquisition device idea. You can define
a specific component, e.g. FooComponent that would generalized the
concept of FooDevice and have some parameters like name, connection
parameters etc… define in a text/xml file. Then, you create a specific
action for FooComponent to start the acquisition (or whatever you need
to do to initialize the data or change it on the
fly).

### How to make Save As work on a Component which is neither an Image nor a Mesh

To use the “Save As…” action, CamiTK looks for the extension which
creates your Component. If your SpecialComponent inherits from
ImageComponent or MeshComponent, it will find any Component Extension
saving an Image or a Mesh. However, if your SpecialComponent directly
inherits from Component, the only way it has to find the appropriate
Save method (in your Special Component Extension) is to look at the file
extension of your SpecialComponent. So as soon as you create an instance
of your SpecialComponent, you must give it a file name (using
setFileName()), even if the corresponding file does not exist (e.g. if
your created your SpecialComponent instance thanks to an action) with
the proper extension so that “Save As…” action can find your
SpecialComponentExtension to save it.

### How to add a new class to a CamiTK project

You have added a class in your CamiTK project, and when compiling it,
the class is forgotten by your IDE (no compilation). Simply run
**Cmake** on your project to consider it. Once done, when recompiling
your project, your class will be now examinated.

### \[Signal/Slots Explorer\] How to detect modifications in Component explorer

As far as you work with several components, it could be interesting to
catch some signals to hide or delete some objects you created in an
action for one of the component to avoid unexpected behaviors.

Let us take a pratical example like developing an interactive
vtkSeedWidget action in the viewers for an imageComponent. If you don’t
catch signals, your seeds will stay in the viewers after the deletion of
your component and finally it will crash because events in vtk pipeline
will lost for your seeds. To manage this, you can use two signals which
are **selectionChanged()** (provided by CamiTK) and
**destroyed(QObject\*)** (provided by Qt). The first one is sed to
manage a change in viewers or explorer. With our example, it can be used
to hide or screen our seeds. The second one is use to catch the deletion
of your component. Here, the aim will be to delete seeds to free the
memory. In the constructor, you can connect these methods with dedicated
methods in your code.

``` cpp

// manage the slots
if (!isConnected) {
   QObject::connect(Explorer::getInstance(), SIGNAL(selectionChanged()), this, SLOT(XX()));
   QObject::connect(MyImgComponent, SIGNAL(destroyed(QObject*)), this, SLOT(YY(QObject*)));
   isConnected = true;
}
```

Don’t forget to also manage the disconnection of these methods.

### \[Action\] How to create an action of actions

In the case you want use a series of CamiTK actions in one of your
action before your algorithm, you should set to false the flag that
inform your image has been modified. To do that, perform an action in
your action, change the flag to false and perform your algorithm.

``` cpp

ImageComponent * input = dynamic_cast<ImageComponent *> (performLaplacianSharpeningAction(comp));
input->setModified(false);
myAlgo (input->getImageData());
```

## Compiling

### \[Compilation error\] VtkImageComponent.h: No such file or directory

This file has not been found because the projet ‘component-vtkimage’
installing the headers have not been built yet. It comes from a
dependencies issue. The CamiTK team is working on it. To correctly
compile your project, first build and install the target
‘component-vtkimage’ before yours.

### \[Compilation error\] VtkMeshComponent.h: No such file or directory

This file has not been found because the projet ‘component-vtkmesh’
installing the headers have not been built yet. It comes from a
dependencies issue. The CamiTK team is working on it. To correctly
compile your project, first build and install the target
‘component-vtkmesh’ before yours.

### \[Compilation error\] PMManagerDC.h: No such file or directory

This file has not been found because the projet
‘component-physicalmodel’ installing the headers have not been built
yet. It comes from a dependencies issue. The CamiTK team is working on
it. To correctly compile your project, first build and install the
target ‘component-physicalmodel’ before
yours.

### \[Compilation error\] Error C2752 or/and C2528 on VS2010 when compiling modeling CEP

The problem comes from the additional templates that were added to
Visual Studio 2010. To fix it, std::make\_pair, which doesn’t always
work has to be changed to pair\<\>. Remplace in the file
MMLMonitorDisplayFactory.h (lines 63 and 82).

``` cpp
     mapObjectCreator.insert( std::make_pair<std::string,CreateMonitorDisplayFunctionPointer>(id, &createTheMonitorDisplay<C> ) );
```

by

``` cpp
      mapObjectCreator.insert( std::pair<std::string,CreateMonitorDisplayFunctionPointer>(id, &createTheMonitorDisplay<C> ) );
```

### \[Linking error\] Undefined reference to …

Typically this error occurs at the last stage of compilation : linkage.
The linker checks that all entries points of your binaries that refers
to external libraries are validated. You encounter this error when the
linker cannot find some entries point (one per error) within the
provided libraries at the link stage. Here are the solutions to this
error, from the most encountered to the least:

  - You did not provide the good libraries for the linking. Check
    configuration to see if you are missing some libraries at the link
    stage.
  - You provide the good libraries but they have a different
    configuration that the binary you are compiling. Thus, mostly on
    Windows platform, binaries are not compatible from one compiler to
    one another. 32bits built libraries are not compatible with 64bits
    (and vice versa). For instance, Qt built libraries for VS 2010 are
    not compatible neither with VS 2008 nor VS 2012 (and surely not with
    MinGW). Check your libraries configuration remains the same for all
    your libraries.
  - You did provide all the good libraries with the good configuration,
    but in the wrong order. Indeed, this one is the trickiest: you must
    indicate which libraries to link with your executable / library you
    are compiling in the CORRECT ORDER. This order is from the most
    dependent to your target from the least. Thus, check your CMake
    configuration to verify that order. If it deals with CamiTK
    extensions / libraries or applications, please contact us by an
    [issue](https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK/issues)

### \[Linking error\] Unresolved symbol …

If you face to error like \*unresolved external symbol "public:
\_\_cdecl\* when you compile with Visual Studio, read this page: [DLL
import/export](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Advanced%20Topics/DLL_Import_Export/).

### \[Linking error\] Relocation R\_X86\_64\_32S against \`.rodata’

This error comes when you tried to link static library on a 64 bits
platform. The solution is to compile the libraries you are linking
against as “shared”. To do so, simply reconfigure your shared lib with
the build\_shared\_lib flag set to on and compile them.

For more information please read the topic on
[StackOverflow](http://stackoverflow.com/questions/9637551/relocation-r-x86-64-32s-against-rodata-while-compiling-on-64-bit-platform)

### \[Install Local error\] VS doesn’t find your CEP to perform a local install

Please verify that the name of your CEP has no space between words. For
example, in VS2008, the local installation of “my CEP” will failed
because VS2008 try to install “my” and “CEP”.

## Running

### Why does my action do not apply to my custom component ?

When creating a new action you will certainly configure a custom
component to apply your action on. Each CamiTK action works on a
specified type of component. In the constructor of your custom action
`MyAction` you will have to explicitly specify which component it works
on, using `setComponent()` method, such as described in the example
below :

``` cpp
MyAction::MyAction(ActionExtension * extension) : Action(extension) {
    // Setting name, description and input component
    setName("My Action name");
    setDescription("The action my component does");
    setComponent("MyComponentClassName");
}
```

`MyComponentClassName` should remain the exact class name of your custom
component.
Each time a new component of type `MyComponentClassName` would be
instanciated in your program, all compatible actions would be
automatically available in the action menu.
In fact, if you look at the code of the `apply()` method of your custom
action :

``` cpp
void MyCustomAction::apply() {
    foreach (Component *comp, targetComponents) {
        MyCustomComponent * input = dynamic_cast<MyCustomComponent *> ( comp );
        if(input)
            apply(input);
    }
}
```

The `targetComponents` member contains all the compatible components for
this specified action.

### Why does nothing appear in the IMP console ?

When running **IMP** the console is redirected as another output. If you
wish to have the standard output using the system console when running
IMP, simply add the flag **-noConsole** when running your application.
For instance, on Visual Studio, right click on your project.

1.  select **properties**
2.  go to **configuration properties** \> **Debugging**
3.  select the 2nd field **command Arguments**
4.  add the **-noConsole** flag to it.

Now the IMP console is redirected to your standard console.

Note : in Visual Studio you have 2 different configuration for Debug and
Release launch of your application. Be sure to edit the ones you work
with.

FYI : the 1st field **Command** refers to the command line launched when
you run and instance of your project debugging. You can edit this line
to run IMP for instance if you develop a custom extension for
it.

### Why am I getting the following error: \<The application was unable to start correctly (0xc000007b)\> when I try to run my application?

![0xc000007berror](./assets/0xc000007berror.jpg)

This error likely comes from trying to run a 32 bit windows application
on a 64 bit environment. The most effective solution is to download
[dependency walker](http://www.dependencywalker.com) and check if your
application is calling any 64 bit dll libraries. If so, then replace
these libraries with the corresponding 32 bit versions. See a [detailed
explanation](http://msoos.wordpress.com/2010/09/11/0xc000007b-error-solution).

An example of when this can occur is when you use libxml2 in your
application. libxml2 depends on the zlib1.dll library which may already
exist on your computer in the 64-bit version for another unrelated
program (such as intel wifi, corel windvd, matlab, etc.). Your program
may be linking to this dll, causing the above error message. The
solution is to download the [32-bit zlib
archive](http://xmlsoft.org/sources/win32/) and to place the
**zlib1.dll** in your **libxml\\bin** directory, the **zlib1.h** file in
your **libxml\\include** directory and the **zlib.lib** file in your
**libxml\\lib** directory.

Other case : If it crash when you run debug version only, it may comes
from a bad ucrtbased.dll version. Try to copy ucrtbased.dll x64 version
from Windows Kits\\10\\bin\\ucrtbased.dll to system32 to replace the bad
one. Or do what its better with file moving for get it working (ie add
this file to your path prior the old one for example).

### It does not wok
    -> clean your build -> run CMake -> build your project -> run camitk-imp from your build directory

### Cannot find my new Cpp file in my IDE
    -> run CMake

### "Could not connect slot to signal"
    -> add Q_OBJECT just after the declaration of your class -> re-run CMake

### During link edition on Windows "Cannot find ... implementation"
    -> Add an API file (dll_spec import, etc.) in reused Components or Actions Extensions -> CMake -> Rebuild -> Re-run
    
    
## Configuring / CMaking

### \[KDevelop Error\] Kdevelop 4.3 crash when you try to import CamiTK project (Ubuntu 12.04)

With some configuration, KDevelop seems to crash because the parser
failed to read the CMakeList when you try to import the project in. The
first thing to do is to check if you can obtain a version of KDevelop in
the package manager of your Linux distribution.

If this solution failed or is not possible, a second trick is to
download and compile sources of a trustable KDevelop and Kdevplatform.
For example, in case of Ubuntu 12.04, KDevelop 4.4.1 for Ubuntu 12.10
seems to work. Go to to the KDevelop
[website](http://www.kdevelop.org/), download sources and follow these
helpful [instructions](http://techbase.kde.org/KDevelop4/HowToCompile)
\! Assuming this stage success, you will be able to import CamiTK
project in
KDevelop.

### \[CMake error\]\[Visual Studio 10 Win64\] The C compiler identification is unknown

Your installation of the Windows 7.1 SDK on Visual Studio 2010 is broken
since you (or your computer in your back) have installed the 7.1 SDK
SP1, which breaks up the x64 compiler.
**Solution:** Download and install this [patch for Windows 7.1 SDK x64
compiler](http://msdn.microsoft.com/en-us/windows/bb980924.aspx), click
download and select **VC-Compiler-KB2519277.exe**

