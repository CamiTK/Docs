# Migration guide from 4.0 to 4.1

The main objective of the CamiTK 4.0 was to ensure that CamiTK will stay up to date with its basic dependencies.
Qt4, VTK5 and CMake version 2 became obsolete.
CamiTK 4.0 had therefore to ensure compatibility with Qt5, VTK6 and CMake version 3.

CamiTK 4.1 is the first stable release of the 4.x family. It removes all the 3.x deprecated methods and clean up a lot of obsolete Core library API methods.

This page guides you to all the changes that might require some modification of your own CEP source code.
There is mainly three things that _might_ require your attention:

- the new log system
- some changes in the `ImageComponent` API
- automatic integration test

## Log message

CamiTK 4.1 introduces a [new log system](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Highlighted%20Features/Log-System/), which is more powerful, very flexible and can trace anything on the console as well as in a log file.
The log macros were simplified and this is the only change that will require your attention.

### Before CamiTK 4.1

Before CamiTK 4.1, the log macros (`CAMITK_ERROR` `CAMITK_WARNING` `CAMITK_INFO` and `CAMITK_DEBUG`) did not use C++ and compiler introspection. They required three parameters:

- the first parameter was a `std::string` that states the global context (usually the name of the class)
- the second parameter was a `std::string`that states the local context (usually the name of the method)
- the third one was a `std::ostream&` formatted object that gave the message per se

### From CamiTK 4.1

1. The log macros only requires one `QString` parameter: you now need to use `QString` concatenation and powerful formatting abilities to create the message. The global and local context are now automatically deduced by the compiler and `Qt` introspection mechanism. You just need to focus on the message.
2. The `CAMITK_DEBUG` macro was renamed `CAMITK_TRACE`
3. The `CAMITK_STATIC_...` macros were renamed `CAMITK_..._ALT` (`ALT` as in alternative)
4. You may have to add `#include <Log.h>` at the top of your implementation files.

It is **recommanded** to read the [new log system documentation](https://camitk.gricad-pages.univ-grenoble-alpes.fr/Docs/Highlighted%20Features/Log-System/) to see how things got much easier to send a message to the user or to debug your code.

### Example

Before CamiTK 4.1

```cpp
CAMITK_WARNING("ReorientImage", "buildGeometries", "3D Model VTK files not found")

...

CAMITK_DEBUG ( "ComputeNormals", "apply", "The mesh point set of " << getTargets().first()->getName().toStdString() << " is not a polydata" )
```

From CamiTK 4.1

```cpp
CAMITK_WARNING("3D Model VTK files not found")

...

CAMITK_TRACE("The mesh point set of " + getTargets().first()->getName() + " is not a polydata")

```

When you will run the updated code, we believe that you'll will quickly see the benefits of the new log system...

## `ImageComponent` API

The following methods in `ImageComponent` were marked as deprecated in CamiTK 3.4 and are now removed:

- the methods `getImageSize()` was removed, use the `"Image Size"` property (e.g., `imageComponent->property("Image Size")`) or directly something like `double* dimensions = imageComponent->getImageData()->GetDimensions()` if you need the image dimensions as an array of `double`.
- the method `getDataType()`  was removed, use the `"Voxel Data Type"` property instead (or directly the `imageComponent->getImageData()->GetScalarTypeAsString()`)
- the method `getVoxelSize()` was removed, use the `"Voxel Size"` property
or directly something like `double* spacing = imageComponent->getImageData()->GetSpacing()`
- the methods `getViewIn3D()` and `setViewIn3D( bool )` were removed, use the `"Display Image in 3D Viewer"` property to get/set the visibility in the 3D viewer
- the method `getImageOrigin()` was removed, use the `Frame` API to get the spacial origin of the image


## Other CamiTK Core library API changes

There were also some internal code cleaning and removal of deprecated methods inside the CamiTK core library. Only the most advance CEP, if any, might be impacted.
It is __very unlikely__ that you will encounter a problem with these removal or renaming.

- In `ActionWidget`
    - `updateTargets()` was renamed into `update()` to better reflect the aims of this method: the targets _and_ the properties are updated
- In `InterfaceBitmap`:
    - Enum `PossibleOrientation` was removed (deprecated in CamiTK 3.4). Use Enum `SliceOrientation` of class `Slice`
    - Methods `get/setRotationX()` `get/setRotationY()` and `get/setRotationZ()` were removed, they were not needed here and should only be required/implemented in `ArbitrarySlice`
- In `InterfaceProperty`:
    - methods `isInstanceOf()` and `getHierarchy()` became `const` (and therefore all `Component`)
    - method `setDynamicProperty()` was removed, as the `CamiTK` property mechanism takes over all the Qt dynamic property management. Use `addProperty(Property *)` instead.
    - method `getPropertyWidget()` (deprecated since CamiTK 3) was removed, use the methods `getPropertyWidgetAt()` and `getNumberOfPropertyWidget()` instead.
    - method `getPropertyWidgetAt()` now only takes one parameter (the second parameter was never used/needed)
- In `MeshComponent`:
    - `setActiveData()` was removed (this method was only available in CamiTK 3.4 to 4.0.4),  use `setDataRepresentationVisibility(fieldType, name, true)` method instead.
    - `getSelectionList()` was removed (this method was only available in CamiTK 3.4.0), use the `getSelections()` methods instead.
- In `Slice` and `SingleImageComponent` accordingly to the removal of `PossibleOrientation`, the corresponding constructors were removed. Use the constructors that take a `SliceOrientation` instead.
- In `Component`
    - accordingly to the `InterfaceProperty` and `InterfaceBitmap` updates, the methods `getPropertyWidget()` `setDynamicProperty()` `get/setRotationX()` `get/setRotationY()` and `get/setRotationZ()` were removed.
    - `getActionAndPopupMenu()` was removed. The `Action` framework in CamiTK 3.0 made this method obsolete.
- In `ImageComponent` and `MeshComponent` accordingly to the `InterfaceProperty` update:
    - `getPropertyWidgetAt()` method now only takes one parameter (the second parameter was never used/needed)
- In `SingleImageComponent`, the method `getMyParentComponent()` was also removed. use `(camitk::Component*) myParentNode` instead.
- In `ActionViewer` class, `getActionViewer()`  was remove (use `getInstance()` instead)
- Class `ComponentPopupMenu` was removed. The `Action` framework in CamiTK 3.0 made this class obsolete.
- In `MainWindow`
    - method `addViewer()` became `private`, use `addDockViewer` or `setCentralViewer` instead
- In `ExtensionManager`
    - methods `loadComponentExtension()` `loadActionExtension()` were removed, use `loadExtension()` instead
    - methods `autoloadComponentExtensions()` and `autoloadActionExtensions()` were removed, use `autoload()` instead
    - methods `getActionExtensions()` `getComponentExtensions()` and `getDataDirectoryComponents()` were removed, use `getActionExtensionsList()` `getComponentExtensionsList()` and `getDataDirectoryComponentsList()` instead
- In `Core`
    - method `getComponentDir()` was removed, use `getComponentDirectories()` instead
    - method `getActionDir()` was removed, use `getActionDirectories()` instead
    - method `getModuleDir()` was removed, use `getComponentDirectories()` or `getActionDirectories()` instead
    - method `getCoreLibDir()` was removed, use `getGlobalInstallDir()` instead

## Command line arguments

Command line arguments were rationalized to conform to GNU standard for command line interface:

- In `camitk-imp` command line argument `--noConsole` was renamed `--no-console`
- In `camitk-actionstatemachine` command line argument `--outDir` was renamed `--output-dir`
- In `camitk-config`:
 	- command line argument `--printPath` was renamed `--print-paths`
 	- command line argument `--camitkDir` was renamed `--camitk-dir`
 	- command line argument `--shortVersion` was renamed `--short-version`
 	- command line argument `--completeVersion` was renamed `--complete-version`
 	- command line argument `--timeStamp` was renamed `--time-stamp`
- In `camitk-testcomponents`
 	- command line argument `--odir` was renamed `--output-dir`

## CMake target names

All the CMake targets created by CamiTK now starts with `camitk` and words are separated using `-` (dash) instead of `_` (underscore).

For instance `sdk_local_install` became `camitk-ce-local-install` ("ce" stands for "community edition", see [next section](#camitk-opensource-cep-set)).

All CEP CMake general targets now start with `camitk-lowercasenameofcep`.

## CamiTK OpenSource cep set

The CamiTK OpenSource cep set was renamed "CamiTK Community Edition" for better identification and clarity.

## Automatic integration test

CamiTK comes with a test builder that can automatically add integration tests for actions and components.
Since version 4.1, the `camitk_extension(..)` macro option that automatically generate these tests was renamed from `AUTO_TEST` to `ENABLE_AUTO_TEST`.

### Before CamiTK 4.1

```cmake
camitk_extension(...
                 AUTO_TEST
                 ...)
```

### Since CamiTK 4.1


```cmake
camitk_extension(...
                 ENABLE_AUTO_TEST
                 ...)
```

!!! Note
    There is also a new integration test, based on the action state machine and that can run an entire pipeline, can also be generated automatically (check the new `ENABLE_INTEGRATION_TEST` option of the `camitk_extension(..)` macro.
